## Synopsis

This is the frontend project for the WhereHouse web app, right now written with Angular 1.6 with grunt as task pipeline and Jasmine for testing.


## Motivation

The motivation for Angular 1.6 is - it's still a js solution (I hate ts!!!) and I know it :) I think in the future we should switch to something newer, but for the mvp it will do! 

## Installation

Go to the frontend/src folder

```
1. npm install
2. npm install grunt-cli -g
3. npm install http-server -g
```

## To run the app

Go to the frontend/src folder

```
1. http-server
2. grunt build
3. grunt watch
```

## Tests

```
grunt eslint //static code analysis for all js files.
grunt jasmine:test_all //run all tests under our tests folder.
``` 