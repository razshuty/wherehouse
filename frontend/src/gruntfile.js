module.exports = function(grunt) {
  
  // Project configuration.
  grunt.initConfig({
    jasmine: {
      test_all: {
        src: 'tests/**/*.js',
        options: {
          specs: '*Spec.js',
          helpers: '*Helper.js'
        }
      }
    },
    eslint: {
      options: {
        configFile: 'config/eslint.json'
      },
      target: [
        'js/*.js', 
        'js/**/*.js',  
        '!js/**/template.js', 
        '!js/router.js',
        '!js/modules/calendar.js',
        '!js/controllers/ui-bootstrap.controller.js',
        '!js/services/inventory.service.js',
        '!js/services/user.service.js',
        '!js/services/customer.service.js',
        '!js/services/supplier.service.js',
        '!js/services/location.service.js',
        '!js/services/technician.service.js',
        '!js/controllers/main.controller.js',
      ]
    },
    copy: {
      publish_config: (function() { 
        var env = grunt.option('target') || 'stage';
        if (env) {
          env = env.toLowerCase();
        }
        if (env === 'prod') {
          return {
            files: [
              {expand: false, src: ['./config/prod/config.js'], dest: './publish/config/config.js', filter: 'isFile'},
            ]
          };
        } else if (env === 'stage') {
          return {
            files: [
              {expand: false, src: ['./config/stage/config.js'], dest: './publish/config/config.js', filter: 'isFile'},
            ]
          };
        } else {
          return {
            files: [
              {expand: false, src: ['./config/local/config.js'], dest: './publish/config/config.js', filter: 'isFile'},
            ]
          };
        }
      }()),     
      publish_frontend: {
        files: [
          {expand: true, src: ['./img/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./css/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./data/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./fonts/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./media/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./template/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./views/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./vendors/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./js/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./node_modules/angular/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./node_modules/angular-animate/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./node_modules/angular-translate/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./node_modules/ng-if-bootstrap-grid/**'], dest: './publish/', filter: 'isFile'},
          {expand: true, src: ['./*.html'], dest: './publish/', filter: 'isFile'}
          
        ],
      },
    },
    pkg: grunt.file.readJSON('package.json'),
    less: {
      development: {
        options: {
          paths: ["css"]
        },
        files: {
          "css/app.css": "less/app.less",
        },
        cleancss: true
      }
    },
    csssplit: {
      your_target: {
        src: ['css/app.css'],
        dest: 'css/app.min.css',
        options: {
          maxSelectors: 4095,
          suffix: '.'
        }
      },
    },
    ngtemplates: {
      whereHouse: {
        src: ['template/**.html', 'template/**/**.html'],
        dest: 'js/templates.js',
        options: {
          htmlmin: {
            collapseWhitespace: true,
            collapseBooleanAttributes: true
          }
        }
      }
    },
    watch: {
      a: {
        files: ['less/**/*.less'], // which files to watch
        tasks: ['less', 'csssplit'],
        options: {
          nospawn: true
        }
      },
      b: {
        files: ['template/**/*.html'], // which files to watch
        tasks: ['ngtemplates'],
        options: {
          nospawn: true
        }
      }
    }
  });
  
  // Load the plugin that provides the "less" task.
  grunt.loadNpmTasks('grunt-csssplit');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks("grunt-eslint");
  
  // Default task(s).
  grunt.registerTask('default', ['less']);
  grunt.registerTask('build', ['less', 'csssplit', 'ngtemplates', 'watch']);
  grunt.registerTask('test', ['eslint', 'jasmine']);

  
};
