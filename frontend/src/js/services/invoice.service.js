whereHouse.service('invoiceService', ['$http', function($http){
  
  //this.invoiceList = $http('...').get();
  
  this.invoiceList = {  
    "list":[
      {  
        "id":1,
        "invoiceNumber": 5676,
        "date":"2012-04-23T18:25:43.511Z",
        "totalPrice":120,
        "currency":"$",
        "remarks":"For the past orders",
        "billingDetails": {
          "name":"Raz Shuty",
          "address":"Bla bla street, Berlin",
          "email":"raz@raz.raz",
          "phone":"01675463789"
        },
        "billedItems":[{
          "id": 1,
          "title":"Apple Keyboard",
          "description":"A Keyboard, you know... for typing ;)",
          "unitPrice":120,
          "quantity": 1
        }]
      },
      {  
        "id":2,
        "invoiceNumber": 5677,
        "date":"2017-07-22T18:25:43.511Z",
        "totalPrice":890,
        "currency":"$",
        "remarks":"For work done ",
        "billingDetails": {
          "name":"Thiago Lunardi",
          "address":"Bla bla street, Berlin",
          "email":"Thiago@ThiagoLunardi.net",
          "phone":"01675463789"
        },
        "billedItems":[
          {
            "id": 1,
            "title":"Apple Keyboard",
            "description":"A Keyboard, you know... for typing ;)",
            "unitPrice":120,
            "quantity": 1
          },
          {
            "id": 2,
            "title":"Mac Mini",
            "description":"Mini desktop by apple, you know... so you could do stuff online",
            "unitPrice":670,
            "quantity": 1
          }]
      },
      {  
        "id":3,
        "invoiceNumber": 5678,
        "date":"2017-12-08T18:25:43.511Z",
        "totalPrice":1460,
        "currency":"$",
        "remarks":"Bla bla bla, selling is bla bla",
        "billingDetails": {
          "name":"Anastasia Zakharova",
          "address":"Bla bla street, Berlin",
          "email":"Anastasia@Zakharova.bla",
          "phone":"01675463789"
        },
        "billedItems":[
          {
            "id": 1,
            "title":"Apple Keyboard",
            "description":"A Keyboard, you know... for typing ;)",
            "unitPrice":120,
            "quantity": 1
          },
          {
            "id": 2,
            "title":"Mac Mini",
            "description":"Mini desktop by apple, you know... so you could do stuff online",
            "unitPrice":670,
            "quantity": 2
          }]
      },
    ]
  };
  
  //temp...
  this.tempId = 3;
  this.tempInvoiceNumber = 5678;
  this.deleteInvoice = function(invoiceId) {
    //http call;
  };
  
  this.getAllInvoices = function() {
    //http call;
    return this.invoiceList;
  };
  
  this.createInvoice = function(item) {
    //http call;
    this.tempId = this.tempId + 1;
    this.tempInvoiceNumber = this.tempInvoiceNumber + 1;
    item['id'] = this.tempId;
    item['invoiceNumber'] = this.tempInvoiceNumber;
    this.invoiceList.list.push(item);
  };
}]);