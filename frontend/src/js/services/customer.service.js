whereHouse.service('customerService', function($http, $window, CONFIG) {
  
  //this.customersList = $http('...').get();
  
  this.customersList = {  
    "list":[
      {  
        "id":1,
        "firstName": 'Raz',
        "lastName": 'Shuty',
        "address": "",
        "email": 'raz.shuty@wherehouse.com',
        "phoneNumber": '015750753120'
      },
      {  
        "id":2,
        "firstName": 'Tal',
        "lastName": 'Shachar',
        "address": "",
        "email": 'Tal.Shachar@wherehouse.com',
        "phoneNumber": '015750753121'
      },
      {  
        "id":3,
        "firstName": 'Christian',
        "lastName": 'Thomae',
        "address": "",
        "email": 'Christian.Thomae@wherehouse.com',
        "phoneNumber": '015750753123'
      },
    ]
  };
  
  //temp...
  this.tempId = 3;
  
  this.deleteCustomer = function(customerId) {
    this.customersList.list = this.customersList.list.filter(customer => customer.id !== customerId);
  };
  
  this.getAllCustomers = function() {
    //http call;
    //return this.customersList;

    var request = {
      method: 'GET',
      url: CONFIG.baseUrl + '/api/v1/customers',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $window.localStorage.getItem("currentUser").token
      }
    };

    $http(request).success(function (response) {
      this.customersList = response.data;
    }).error(function (error) {
      console.error(error);
    });

  };
  
  this.addNewCustomer = function(newCustomer) {
    //http call;
    this.tempId = this.tempId + 1;
    newCustomer['id'] = this.tempId;
    this.customersList.list.push(newCustomer);
  };
});