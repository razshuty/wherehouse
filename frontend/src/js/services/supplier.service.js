whereHouse.service('supplierService', ['$http', function($http){
  whereHouse.service('supplierService', function($http) {  
    
  //this.supplierList = $http('...').get();
  
  this.supplierList = {  
    "list":[
      {  
        "id":1,
        "firstName": 'Raz',
        "lastName": 'Shuty',
        "address": "",
        "email": 'raz.shuty@wherehouse.com',
        "phoneNumber": '015750753120'
      },
      {  
        "id":2,
        "firstName": 'Tal',
        "lastName": 'Shachar',
        "address": "",
        "email": 'Tal.Shachar@wherehouse.com',
        "phoneNumber": '015750753121'
      },
      {  
        "id":3,
        "firstName": 'Christian',
        "lastName": 'Thomae',
        "address": "",
        "email": 'Christian.Thomae@wherehouse.com',
        "phoneNumber": '015750753123'
      },
    ]
  };
  
  //temp...
  this.tempId = 3;
  
  this.deleteSupplier = function(supplierId) {
    this.supplierList.list = this.supplierList.list.filter(supplier => supplier.id !== supplierId);
  };
  
  this.getAllSuppliers = function() {
    //http call;
    return this.supplierList;
  };
  
  this.addNewSupplier = function(newSupplier) {
    //http call;
    this.tempId = this.tempId + 1;
    newSupplier['id'] = this.tempId;
    this.supplierList.list.push(newSupplier);
  };
}]);