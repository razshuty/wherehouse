whereHouse.service('todoService', ['$resource', function($resource){
  this.getTodo = function(todo) {
    var todoList = $resource('data/todo.json');
    
    return todoList.get({
      todo: todo
    });
  };
}]);
