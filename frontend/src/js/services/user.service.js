whereHouse.service('userService', ['$http', function($http){

  //this.usersList = $http('data/user.json').get();

  this.usersList = {
    "list":[
      {
        "id":1,
        "email": "blabla@msms.cxom",
        "firstName": "Chicka",
        "lastName": "Buna",
        "role": 1,
        "registered":true
      },
      {
        "id":2,
        "email": "blabla@mssms.cxom",
        "firstName": "Nata",
        "lastName": "lmlml",
        "role": 0,
        "registered":true
      },
      {
        "id":3,
        "email": "ddd@msms.cxom",
        "firstName": "ddd",
        "lastName": "dddd",
        "role": 0,
        "registered":true
      }
    ]
  };

  this.tempId = 3;
  this.getAllUsers = function() {
    return this.usersList;
  };

  this.deleteUser = function(user) {
    this.usersList.list = this.usersList.list.filter(x => x.id !== user.id);
    return this.usersList;
  };

  this.updateUser = function(user) {
    //update http user
    this.usersList.list[user.id-1] = user;
  }

  this.inviteUser = function(user) {
    this.tempId = this.tempId +1;
    this.usersList.list.push({
      "id": this.tempId,
      "email": user.email,
      "firstName": user.firstName,
      "lastName": "",
      "role": 0,
      "registered":false
    });
  };
}]);
