whereHouse.service('bestsellingService', ['$resource', function($resource){
  this.getBestselling = function(img, name, range) {
    var gbList = $resource('data/best-selling.json');
    
    return gbList.get({
      img: img,
      name: name,
      range: range,
    });
  };
}]);
