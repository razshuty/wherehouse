whereHouse.service('locationService', function($http, CONFIG) {
  
  this.locationList = {  
    "list":[
      {  
        "id":1,
        "name": "Storage Unit 1",
        "description": "A storage unit!"
      },
      {  
        "id":2,
        "name": "Storage Unit 2",
        "description": "A storage unit!"
      },
      {  
        "id":3,
        "name": "Storage Unit 3",
        "description": "A storage unit!"
      }
    ]
  };
  
  //temp...
  this.tempId = 3;
  
  this.deleteLocation = function(locationId) {
    this.locationList.list = this.locationList.list.filter(location => location.id !== locationId);
  };
  
  this.getAllLocations = function(callback) {
    return this.locationList;
  };
  
  this.createLocation = function(location) {
    //http call;

    $http({
      method: 'POST',
      url: CONFIG.baseUrl + '/api/v1/Locations',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        id: 123,
        name: 'raz'
      }
    }).then(function successCallback(response) {
      callback(response);
    }, function errorCallback(response) {
      console.log(response);
    });

    this.tempId = this.tempId + 1;
    location['id']  = this.tempId;
    this.locationList.list.push(location);
  };
});