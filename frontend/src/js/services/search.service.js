whereHouse.service('searchService', function($http, CONFIG) {  

  this.performSearch = function(searchQuery) {  
    var request = {
      method: 'GET',
      url: CONFIG.baseUrl + '/api/v1/search',
      headers: {
        'Content-Type': 'application/json'
      }
    };
    
    $http(request).success(function (response) {
      return response.data;
    }).error(function (error) {
      console.log(response);
    });
  };
  
});
