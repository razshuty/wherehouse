whereHouse.service('recentpostService', ['$resource', function($resource){
  this.getRecentpost = function(img, user, text) {
    var recentpostList = $resource('data/messages-notifications.json');
    
    return recentpostList.get ({
      img: img,
      user: user,
      text: text
    });
  };
}]);