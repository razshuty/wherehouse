whereHouse.service('technicianService', function($http) {

  this.tasksList = {
    "list":[
      {
        "id":1,
        "img": "http://cdn.mos.cms.futurecdn.net/df65adee6e59a7b4dbf9a41b68c8fe38-1200-80.jpg",
        "type": "iPhone 6",
        "description": "Screen is broken",
        "arrivalDate": "2017-03-25",
        "status": "In Progress",
        "customer": {},
        "repair": {},
        "technician": "Tal",
        "startworkingat": "2017-09-25"
      },
      {
        "id":2,
        "img": "https://skattercdn.azureedge.net/files/2011/06/samsung-nexus-s-4g-sprint.jpg",
        "type": "Samsung Nexus S",
        "description": "Battery dying fast",
        "arrivalDate": "2017-09-25",
        "status": "In Progress",
        "customer": {}
      },
      {
        "id":3,
        "img": "http://hampdencomputer.com/wp-content/uploads/2013/09/macfix.jpg",
        "type": "Macbook Pro",
        "description": "Black screen every couple of minutes. Customer says it happens usually when he Mac is working hard.",
        "arrivalDate": "2017-10-01",
        "status": "Waiting",
        "customer": {}
      },
    ]
  };

  //temp...
  this.tempId = 3;

  this.deleteTask = function(taskId) {
    this.supplierList.list = this.tasksList.list.filter(task => task.id !== taskId);
  };

  this.getAllOpenTasks = function() {
    //http call;
    return this.tasksList;
  };

  this.addNewTask = function(newTask) {
    //http call;
    this.tempId = this.tempId + 1;
    newTask['id'] = this.tempId;
    newTask['status'] = "New";
    newTask['img'] = "http://cdn.mos.cms.futurecdn.net/df65adee6e59a7b4dbf9a41b68c8fe38-1200-80.jpg";
    this.tasksList.list.push(newTask);
  };

  this.changeTaskStatus = function(currentTask, newStatus) {
    currentTask['status'] = newStatus;
  };

  this.assignTechnician = function(currentTask, technician) {
    currentTask['technician'] = technician
  }; 

  this.assignStartTimeStamp = function(currentTask) {
    currentTask['startworkingat'] = Date.now();
  }
});
