whereHouse.service('growlService', function(){
  var gs = {};
  gs.growl = function(message, type) {
    $.growl({
      message: message
    },{
      type: type,
      allow_dismiss: false,
      label: 'Cancel',
      className: 'btn-xs btn-inverse',
      placement: {
        from: 'top',
        align: 'center'
      },
      delay: 3000,
      animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOut'
      },
      offset: {
        x: 20,
        y: 85
      }
    });
  };
  
  return gs;
});
