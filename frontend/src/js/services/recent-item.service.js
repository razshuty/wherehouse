whereHouse.service('recentitemService', ['$resource', function($resource){
  this.getRecentitem = function(id, name, price) {
    var recentitemList = $resource('data/recent-items.json');
    
    return recentitemList.get ({
      id: id,
      name: name,
      price: price
    });
  };
}]);
