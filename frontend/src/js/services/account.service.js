whereHouse.service('accountService', ['$http', function($http){
  
  //this.accountDetails = $http('...').get();
  
  this.accountDetails = {  
    "data": {  
      "id":1,
      "img":"http://pfuclan.co.uk/wp-content/uploads/2015/11/logo-160.png",
      "name": "WhereHouse GmbH",
      "address": "Raumerstr. 16, 10437, Berlin",
      "phone": "0157-50503120",
      "email":"raz@raz.raz",
      "supportEmail":"support@raz.raz",
      "site":"www.wherehouse.de",
    }
  };
  
  this.getAccountDetails = function() {
    //http call;
    return this.accountDetails.data;
  };
  
  this.updateAccountDetails = function(account) {
    //http call;
    this.accountDetails.data = account;
  };
}]);