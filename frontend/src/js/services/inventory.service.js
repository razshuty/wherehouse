whereHouse.service('inventoryService', function($http, CONFIG) {

  //this.inventoryList = $http('...').get();
  
  this.inventoryList = {  
    "list":[
      {  
        
        "id":1,
        "img": "https://images.techhive.com/images/article/2013/05/apple-wireless-keyboard-100038979-orig.png",
        "title": "Magic Keyboard - US English",
        "upc":0,
        "description": "Magic Keyboard combines a sleek design with a built-in rechargeable battery and enhanced key features",
        "category":["Apple", "Keyboard"],
        "currency": "$",
        "purchasePrice": 800,
        "sellingPrice": 1200,
        "quantity": 140,
        "unitOfMeasurement" : "items",
        "multipleItems":true,
        "itemVariation": [
          {
            
          },
          {
            
          }
        ],
      },
      {  
        
        "id":2,
        "img": "https://cdn.shopify.com/s/files/1/0259/1735/products/magic_mouse_skin_matt_black_1_1024x1024.jpg",
        "title": "Magic Mouse 2",
        "description": "Featuring a new design, Magic Mouse 2 is completely rechargeable, so you’ll eliminate the use of traditional batteries.",
        "upc":1,
        "category":["Apple", "Mouse"],
        "currency": "$",
        "purchasePrice": 1999,
        "sellingPrice": 300,
        "quantity": 23,
        "unitOfMeasurement" : "items",
        "multipleItems":true,
        "itemVariation": [
          {
            
          },
          {
            
          }
        ],
      },
      {  
        
        "id":3,
        "img": "http://cdn.mos.cms.futurecdn.net/a89992626a0bf98e5f9030af8584b26e-480-80.jpg",
        "title": "Mac mini",
        "description": "Mac mini is the affordable powerhouse that packs the entire Mac experience into a 19.7 centimeter-square frame. Just connect your own display, keyboard, and mouse, and you’re ready to make big things happen.",
        "upc":2,
        "category":["Apple", "Computer", "Mini Computer"],
        "currency": "$",
        "purchasePrice": 200,
        "sellingPrice": 3200,
        "quantity": 0,
        "unitOfMeasurement" : "items",
        "multipleItems":true,
        "itemVariation": [
          {

          },
          {

          }
        ],
      }
    ]
  };
  
  //temp...
  this.tempId = 3;

  this.deleteItem = function(itemId) {
    //http call;
    this.inventoryList.list = this.inventoryList.list.filter(item => item.id !== itemId);
  };

  this.getAllItems = function() {
    //http call;
    return this.inventoryList;
  };

  this.queryItem = function(itemId, callback) {
    var request = {
      method: 'GET',
      url: CONFIG.baseUrl + '/api/v1/upcitem/' + itemId,
      headers: {
        'Content-Type': 'application/json'
      }
    };

    $http(request).success(function (response) {
      callback(response.data);
    }).error(function (error) {
      callback(false, error);
    });

  }
  this.addItem = function(item) {
    //http call;
    this.tempId = this.tempId + 1;
    this.inventoryList.list.push({
      "id":this.tempId + 1,
      "img": "http://www.cutestpaw.com/wp-content/uploads/2011/11/friend.jpg",
      "title": item.title,
      "description": item.description,
      "upc": item.upc,
      "category":item.category,
      "currency": item.currency,
      "purchasePrice": item.purchasePrice,
      "sellingPrice": item.sellingPrice,
      "quantity": item.quantity,
      "unitOfMeasurement" : item.unitOfMeasurement,
      "multipleItems":item.multipleItems,
    });
  };
});