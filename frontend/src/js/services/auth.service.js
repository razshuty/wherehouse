whereHouse.service('authService', function($http, $window, $location, CONFIG) {

  this.logout = function () {
    delete $window.localStorage.currentUser;
    $http.defaults.headers.common.Authorization = '';
    $location.path('/login');
  };

  this.isAuthenticated = function () {
    var currentUser = JSON.parse($window.localStorage.getItem("currentUser"));
    if (currentUser && currentUser.token) {
      $http.defaults.headers.common.Authorization = 'Bearer ' + currentUser.token;
    } else {
      $location.path('/login');
    }
  };

  this.createNewUser = function(account, email, password, callback) {
    var request = {
      method: 'POST',
      url: CONFIG.authDetails.authUrl + '/account/register',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'account': account,
        'email': email,
        'password': password,
        'confirmPassword': password
      }
    };

    $http(request).success(function (response) {
      callback(true);
    }).error(function (error) {
      callback(false, error);
    });
  };

  this.resetPassword = function(email, callback) {
    var request = {
      method: 'POST',
      url: CONFIG.baseUrl + '/api/v1/account/reset',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'email': email
      }
    };

    $http(request).success(function (response) {
      callback(true);
    }).error(function (error) {
      callback(false, error);
    });
  };

  this.login = function(email, password, rememberMe, callback) {
    var request = {
      method: 'POST',
      url: CONFIG.authDetails.authUrl + '/connect/token',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: CONFIG.authDetails.authData +
            "&username=" + encodeURIComponent(email) +
            "&password=" + encodeURIComponent(password) 
    };

    $http(request).success(function (response) {
      if (response.access_token) {
        $window.localStorage.setItem("currentUser", angular.toJson({ username: email, token: response.access_token, role: 1 }));
        $http.defaults.headers.common.Authorization = response.token_type + ' ' + response.access_token;
        callback(true);
      } else {
        callback(false);
      }
    }).error(function (error) {
      callback(false, error);
    });
  };

});
