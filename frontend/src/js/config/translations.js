var translationsEN = {
  HEADLINE: 'What an awesome module!',
  PARAGRAPH: 'Srsly!',
  PASSED_AS_TEXT: 'Hey there! I\'m passed as text value!',
  PASSED_AS_ATTRIBUTE: 'I\'m passed as attribute value, cool ha?',
  PASSED_AS_INTERPOLATION: 'Beginners! I\'m interpolated!',
  VARIABLE_REPLACEMENT: 'Hi {{name}}',
  MISSING_TRANSLATION: 'Oops! I have not been translated into German...',
  BUTTON_LANG_DE: 'German',
  BUTTON_LANG_EN: 'English',
  LANGUAGE: 'Language',
  SELECTED_LANGUAGE: 'img/headers/flags/us.svg',
  COPYRIGHT_TEXT: 'Copyright © 2017 WhereHouse',

  //Menu
  INVENTORY_TAB: 'Inventory',
  LOCATIONS_TAB: 'Locations',
  INVOICES_TAB: 'Invoices',
  DASHBOARD_TAB:'Dashboard',
  CUSTOMERS_TAB: 'Customers',
  SUPPLIER_TAB: 'Suppliers',
  USER_PROFILE_TAB: 'User Profile',
  ACCOUNT_SETTINGS_TAB: 'Account Settings',
  TECHNICIAN_TAB : 'Technician Loop',
  
  //Manage users
  MANAGE_USERS_TITLE: 'Manage Users',

  //Dashboard
  DASHBOARD_VIEW: 'DASHBOARD',
  BEST_SELLING: 'Best selling items',
  LOW_STOCK: 'Low stock items',
  ITEMS_IN_STOCK: 'Items in stock',
  TO_BE_RECEIVED: 'Items to be received',
  ITEMS_SOLD: 'Items sold',
  CURRENT_VALUE: 'Current value in stock',
  EXPECTED_VALUE: 'Expected value in stock',
  TOTAL_VALUE: 'Total sales',

  //Inventory
  INVENTORY_VIEW: 'Inventory',
  ITEM_ADDED: 'Item was added to your inventory',
  ITEM_NOT_ADDED: 'Item was not added',

  //Invoices
  INVOICES_VIEW: 'Invoices',

  //Supplier
  SUPPLIERS_VIEW: 'Suppliers',
  ADD_NEW_SUPPLIER: 'Add New Supplier',
  NEW_SUPPLIER_TITLE: 'Add New Supplier To System',

  //Locations
  LOCATIONS_VIEW: 'Locations',
  ADD_NEW_LOCATION: 'Add new location',
  NEW_LOCATION_TITLE: 'Add New Location To System',

  //Customers
  CUSTOMERS_VIEW: 'Customers',
  ADD_NEW_CUSTOMER: 'Add New Customer',
  NEW_CUSTOMER_TITLE: 'Add New Customer To System'
};

var translationsDE = {
  HEADLINE: 'Was für ein großartiges Modul!',
  PARAGRAPH: 'Ernsthaft!',
  PASSED_AS_TEXT: 'Hey! Ich wurde als text übergeben!',
  PASSED_AS_ATTRIBUTE: 'Ich wurde als Attribut übergeben, cool oder?',
  PASSED_AS_INTERPOLATION: 'Anfänger! Ich bin interpoliert!',
  VARIABLE_REPLACEMENT: 'Hi {{name}}',
  // MISSING_TRANSLATION is ... missing :)
  BUTTON_LANG_DE: 'Deutsch',
  BUTTON_LANG_EN: 'Englisch',
  LANGUAGE: 'Sprache',
  SELECTED_LANGUAGE: 'img/headers/flags/de.svg',
  COPYRIGHT_TEXT: 'Copyright © 2017 WhereHouse',

  //Menu
  INVENTORY_TAB: 'Lagerbestand',
  LOCATIONS_TAB: 'Standorte',
  INVOICES_TAB: 'Rechnungen',
  DASHBOARD_TAB:'Dashboard',
  CUSTOMERS_TAB: 'Kunden',
  SUPPLIER_TAB: 'Lieferanten',
  USER_PROFILE_TAB: 'Benutzerprofil',
  ACCOUNT_SETTINGS_TAB: 'Kontoeinstellungen',
  TECHNICIAN_TAB : 'Techniker Loop',
  
  //Manage users
  MANAGE_USERS_TITLE: 'Benutzer verwalten',

  //Dashboard
  DASHBOARD_VIEW: 'DASHBOARD',
  BEST_SELLING: 'Bestseller',
  LOW_STOCK: 'Niedrige Bestände',
  ITEMS_IN_STOCK: 'Artikel auf Lager',
  TO_BE_RECEIVED: 'Zu empfangende Artikel',
  ITEMS_SOLD: 'Verkaufte Artikel',
  CURRENT_VALUE: 'Aktueller Lagerwert',
  EXPECTED_VALUE: 'Zu erwartender Lagerwert',
  TOTAL_VALUE: 'Gesamtumsatz',

  //Inventory
  INVENTORY_VIEW: 'Lagerbestand',
  ITEM_ADDED: 'Artikel wurde dem Lagerbestand hinzugefügt',
  ITEM_NOT_ADDED: 'Artikel wurde nicht hinzugefügt',

  //Invoices
  INVOICES_VIEW: 'Rechnungen',

  //Suppliers
  SUPPLIERS_VIEW: 'Lieferanten',
  ADD_NEW_SUPPLIER: 'Neuen Lieferanten hinzufügen',
  NEW_SUPPLIER_TITLE: 'Neuen Lieferanten zum System hinzufügen',

  //Locations
  LOCATIONS_VIEW: 'Standorte',
  ADD_NEW_LOCATION: 'Neuen Standort hinzufügen',
  NEW_LOCATION_TITLE: 'Neuen Standort zum System hinzufügen',

  //Customers
  CUSTOMERS_VIEW: 'Kunden',
  ADD_NEW_CUSTOMER: 'Add New Customer',  //missing translation
  NEW_CUSTOMER_TITLE: 'Add New Customer To System'
};

whereHouse.config(['$translateProvider', function ($translateProvider) {
  // add translation tables
  $translateProvider.translations('de', translationsDE);
  $translateProvider.translations('en', translationsEN);
  $translateProvider.preferredLanguage('de');
  $translateProvider.fallbackLanguage('de');
}]);
