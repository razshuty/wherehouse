whereHouse.controller('headerCtrl', function($timeout, $location, $translate, messageService, authService){

  this.signOut = function () {
    authService.logout();
  };

  // Top Search

  // Get messages and notification for header
  this.img = messageService.img;
  this.user = messageService.user;
  this.user = messageService.text;

  this.messageResult = messageService.getMessage(this.img, this.user, this.text);

  this.accountSettings = function() {
    $location.path('/account/account-settings');
  };

  this.userProfile = function() {
    $location.path('/account/user-profile');
  };
  

  //Clear Notification
  this.clearNotification = function($event) {
    $event.preventDefault();

    var x = angular.element($event.target).closest('.listview');
    var y = x.find('.lv-item');
    var z = y.size();

    angular.element($event.target).parent().fadeOut();

    x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
    x.find('.grid-loading').fadeIn(1500);
    var w = 0;

    y.each(function(){
      var z = $(this);
      $timeout(function(){
        z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
          z.remove();
        });
      }, w+=150);
    });

    $timeout(function(){
      angular.element('#notifications').addClass('empty');
    }, (z*150)+200);
  };


  //Fullscreen View
  this.fullScreen = function() {
    //Launch
    function launchIntoFullscreen(element) {
      if(element.requestFullscreen) {
        element.requestFullscreen();
      } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
      } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
      }
    }

    //Exit
    function exitFullscreen() {
      if(document.exitFullscreen) {
        document.exitFullscreen();
      } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }

    if (exitFullscreen()) {
      launchIntoFullscreen(document.documentElement);
    }
    else {
      launchIntoFullscreen(document.documentElement);
    }
  };

  this.inventory = function() {
    $location.path('/inventory');
  };

  this.locations = function() {
    $location.path('/locations');
  };

  this.invoices = function() {
    $location.path('/invoices');
  };

  this.dashboard = function() {
    $location.path('/dashboard');
  };

  this.changeLanguage = function (langKey) {
    $translate.use(langKey);
  };
});
