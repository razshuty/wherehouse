
whereHouse.controller('photoCtrl', function() {
  
  //Default grid size (2)
  this.photoColumn = 'col-md-2';
  this.photoColumnSize = 2;
  
  this.photoOptions = [
    { value: 2, column: 6 },
    { value: 3, column: 4 },
    { value: 4, column: 3 },
    { value: 1, column: 12 },
  ];
  
  //Change grid
  this.photoGrid = function(size) {
    this.photoColumn = 'col-md-'+size;
    this.photoColumnSize = size;
  };
  
});