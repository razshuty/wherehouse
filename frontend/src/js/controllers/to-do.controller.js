whereHouse.controller('todoCtrl', function(todoService){
  
  //Get Todo List Widget Data
  this.todo = todoService.todo;
  
  this.tdResult = todoService.getTodo(this.todo);
  
  //Add new Item (closed by default)
  this.addTodoStat = false;
});