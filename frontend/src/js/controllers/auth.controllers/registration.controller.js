whereHouse.controller('registrationCtrl', function ($scope, $location, authService, growlService) {
  this.account;
  this.firstName;
  this.lastName;
  this.password;
  this.email;
  this.licenseAgreement;
  
  this.createNewUser = function() {
    var email = this.email;
    var password = this.password;
    var account = this.account;
    if (!this.licenseAgreement) {
      growlService.growl('Please accept the license agreement!', 'danger');
    } else {
      authService.createNewUser(this.account, this.email, this.password, function (result, reason) {
        if (result) {
          login(email, password, true);
        } else {
          growlService.growl(reason.errors.join('\n'), 'danger');
        }
      }); 
    }
  };

  function login(email, password, rememberMe){
    authService.login(email, password, rememberMe, function (result, reason) {
      if (result) {
        $location.path('/home');
      } else {
        if (reason) {
          growlService.growl(reason.errors.join('\n'), 'danger');
        } else {
          growlService.growl('Login failure', 'danger');
        }
      }
    });
  }

});
