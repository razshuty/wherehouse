whereHouse.controller('loginCtrl', function ($scope, $location, $timeout, animationService, authService, growlService) {
  this.login = 1;
  this.forgot = 0;
  
  this.account;
  this.password;
  this.email;
  this.rememberMe;
  
  //Animate    
  this.ca = '';
  
  this.performLogin = function() {
    login(this.email, this.password, this.rememberMe);
  };
  
  this.resetPassword = function() {
    authService.resetPassword(this.email, function(result, reason) {
      if (result) {
        growlService.growl('Email to reset password has been sent. Please check you inbox', 'success');
      } else {
        growlService.growl('Something Happend!', 'danger');
      }
    });
    this.login = 1;
    this.forgot = 0;
  };
  
  function login(email, password, rememberMe) {
    authService.login(email, password, rememberMe, function (result, reason) {
      if (result) {
        animationService.ca = this.ca;
        animationService.setAnimation('fadeOutRight','fadeingExits', function() {
          $location.path('/home');
        });     
      } else {
        if (reason) {
          growlService.growl(reason.errors.join('\n'), 'danger');
        } else {
          growlService.growl('Login failure', 'danger');
        }
      }
    }); 
  };
  
});
