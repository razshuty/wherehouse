whereHouse.controller('detailLocationCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (location, size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'detailInvoice.html', 'locationsCtrl', location);
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
  
});