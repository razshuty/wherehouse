whereHouse.controller('newLocationCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'newLocationModal.html', 'locationsCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };

});