whereHouse.controller('locationsCtrl', function ($scope, $rootScope, $location, $window, authService, locationService, growlService) {

  authService.isAuthenticated();

  this.allLocations = locationService.getAllLocations();

  $scope.save = function(location) {
    locationService.createLocation(location);
    $rootScope.modal.close();
    growlService.growl('Location was created', 'success');
  };

  this.delete = function (locationId) {
    if ($window.confirm('Are you sure you want to delete this location?')) {
      locationService.deleteLocation(locationId);
    }
  };
    
  $scope.cancel = function () {
    $rootScope.modal.close();
    growlService.growl('Location was not created', 'warning');
  };

});
