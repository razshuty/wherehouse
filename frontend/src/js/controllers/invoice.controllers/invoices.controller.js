whereHouse.controller('invoicesCtrl', function ($scope, $rootScope, invoiceService, accountService, authService, growlService) {
  
  authService.isAuthenticated();
  $scope.searchText = '';
  $scope.allInvoices = invoiceService.getAllInvoices();
  $scope.accountDetails = accountService.getAccountDetails();
  $scope.newInvoice = {
    billedItems: []
  };
  if ($rootScope.modalContent) {
    $scope.modalContent = $rootScope.modalContent;
  }
  
  $scope.saveInvoice = function (invoiceDetails) {
    var total = 0;
    for(var i = 0; i < invoiceDetails.billedItems.length; i++){
      var item = invoiceDetails.billedItems[i];
      total += (item.unitPrice * item.quantity);
    }
    invoiceDetails['totalPrice'] = total;
    invoiceService.createInvoice(invoiceDetails);
    $rootScope.modal.close();
    growlService.growl('Inovice was added to your list', 'success');
  };
  
  $scope.addLine = function () {
    $scope.newInvoice.billedItems.push({});
  }; 

  $scope.cancel = function () {
    $rootScope.modal.close();
    growlService.growl('Inovice was not added', 'warning');
  };
  
});
