whereHouse.controller('detailInvoiceCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (invoice, size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'detailInvoice.html', 'invoicesCtrl', invoice);
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
  
});