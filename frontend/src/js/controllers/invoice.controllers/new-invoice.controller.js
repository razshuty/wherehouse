whereHouse.controller('newInvoiceCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'newInvoice.html', 'invoicesCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };

});