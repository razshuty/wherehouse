whereHouse.controller('homeCtrl', function ($location, searchService, authService) {

  authService.isAuthenticated();

  this.search;

  this.performSearch = function () {
    var response = searchService.performSearch(this.search);
  };

  //Should come from backend! introduce service for it if we decide to use... 
  this.myInterval = 10000;
  this.slides = [
    {
      img: 'c-1.jpg',
      title: 'Welcome to WhereHouse inventory solutions',
      text: 'Let us help you sort your inventory and keep track of your business'
    },
    {
      img: 'c-2.jpg',
      title: 'Go ahead and add your first inventory item under the Inventory Tab',
      text: 'So simple, because we are here for you!'
    },
    {
      img: 'c-3.jpg',
      title: 'Once you start working go ahead and checkout your dashboard',
      text: 'See what is really going on with your stock and what we think could improve'
      
    }
  ];

});
