whereHouse.controller('manageUsersCtrl', function ($location, $scope, $rootScope, $window, authService, userService, growlService) {

  authService.isAuthenticated();

  $scope.allUsers = userService.getAllUsers().list;
  $scope.newUser = {};

  $scope.deleteUser = function(user) {
    if ($window.confirm('Are you sure you want to delete the user?')) {
      userService.deleteUser(user);
      $scope.allUsers = userService.getAllUsers().list;
    }
  };

  $scope.toggleAdmin = function(user) {
    if (user.role === 0) {
      user.role = 1;
    } else {
      user.role = 0;
    }
    userService.updateUser(user);
  };

  $scope.isAdmin = function() {
    var currentUser = JSON.parse($window.localStorage.getItem("currentUser"));
    return currentUser.role === 1;
  };

  $scope.inviteUser = function () {
    userService.inviteUser($scope.newUser);
    $rootScope.modal.close();
    growlService.growl('User will be invited via mail', 'success');
  };

  $scope.cancel = function (item) {
    $rootScope.modal.close();
    growlService.growl('User was not invited', 'warning');
  };
});
