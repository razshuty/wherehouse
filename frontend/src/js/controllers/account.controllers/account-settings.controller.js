whereHouse.controller('accountSettingsCtrl', function ($scope, $location, authService, accountService) {
  
  authService.isAuthenticated();
  this.tabs = [
    {
      name: 'Manage Users',
      content: 'views/pages/account/users/manage-users.html',
      disabled: false,
    }, 
    {
      name: 'Account Details',
      content: 'views/pages/account/users/account-details.html',
      disabled: false,
    }
  ];
  
  this.allSettings = accountService.getAccountDetails();
  
  this.activateManageUsers = function () {
    this.manage = true; 
    this.details = false;
  };
  
  this.activateAccountSettings= function () {
    this.manage = false; 
    this.details = true;
  };
});
