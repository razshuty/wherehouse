whereHouse.controller('addUserCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'addUserModalContent.html', 'manageUsersCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
});