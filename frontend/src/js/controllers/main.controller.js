whereHouse

// =========================================================================
// Best Selling Widget
// =========================================================================

.controller('bestsellingCtrl', function(bestsellingService){
  // Get Best Selling widget Data
  this.img = bestsellingService.img;
  this.name = bestsellingService.name;
  this.range = bestsellingService.range; 
  
  this.bsResult = bestsellingService.getBestselling(this.img, this.name, this.range);
})

// =========================================================================
// Recent Items Widget
// =========================================================================

.controller('recentitemCtrl', function(recentitemService){
  
  //Get Recent Items Widget Data
  this.id = recentitemService.id;
  this.name = recentitemService.name;
  this.parseInt = recentitemService.price;
  
  this.riResult = recentitemService.getRecentitem(this.id, this.name, this.price);
})


// =========================================================================
// Recent Posts Widget
// =========================================================================

.controller('recentpostCtrl', function(recentpostService){
  
  //Get Recent Posts Widget Items
  this.img = recentpostService.img;
  this.user = recentpostService.user;
  this.text = recentpostService.text;
  
  this.rpResult = recentpostService.getRecentpost(this.img, this.user, this.text);
})

//Add event Controller (Modal Instance)
.controller('addeventCtrl', function($scope, $modalInstance, calendarData){
  
  //Calendar Event Data
  $scope.calendarData = {
    eventStartDate: calendarData[0],
    eventEndDate:  calendarData[1]
  };
  
  //Tags
  $scope.tags = [
    'bgm-teal',
    'bgm-red',
    'bgm-pink',
    'bgm-blue',
    'bgm-lime',
    'bgm-green',
    'bgm-cyan',
    'bgm-orange',
    'bgm-purple',
    'bgm-gray',
    'bgm-black',
  ]
  
  //Select Tag
  $scope.currentTag = '';
  
  $scope.onTagClick = function(tag, $index) {
    $scope.activeState = $index;
    $scope.activeTagColor = tag;
  } 
  
  //Add new event
  $scope.addEvent = function() {
    if ($scope.calendarData.eventName) {
      
      //Render Event
      $('#calendar').fullCalendar('renderEvent',{
        title: $scope.calendarData.eventName,
        start: $scope.calendarData.eventStartDate,
        end:  $scope.calendarData.eventEndDate,
        allDay: true,
        className: $scope.activeTagColor
        
      },true ); //Stick the event
      
      $scope.activeState = -1;
      $scope.calendarData.eventName = '';     
      $modalInstance.close();
    }
  }
  
  //Dismiss 
  $scope.eventDismiss = function() {
    $modalInstance.dismiss();
  }
});

