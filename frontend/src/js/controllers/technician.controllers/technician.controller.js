whereHouse.controller('technicianCtrl', function ($window, $rootScope, $scope, authService, technicianService, growlService) {

  authService.isAuthenticated();

  this.allOpenTasks = technicianService.getAllOpenTasks();

  this.deleteTask = function (taskId) {
    technicianService.deleteTask(taskId);
  };

  $scope.cancel = function (item) {
    $rootScope.modal.close();
    growlService.growl('Task was not added', 'warning');
  };

  $scope.save = function (task) {
    technicianService.addNewTask(task);
    $rootScope.modal.close();
    growlService.growl('Task was added to your list', 'success');
  };

  $scope.startTask = function (currentTask) {
    technicianService.changeTaskStatus(currentTask, "In Progress");
    var currentUser = JSON.parse($window.localStorage.getItem("currentUser"));
    technicianService.assignTechnician(currentTask, currentUser.username);
    technicianService.assignStartTimeStamp(currentTask);
    growlService.growl('Task Started', 'success');
  };
});
