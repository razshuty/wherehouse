whereHouse.controller('addTechnicianTaskCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'addTechnicianTaskModalContent.html', 'technicianCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
});