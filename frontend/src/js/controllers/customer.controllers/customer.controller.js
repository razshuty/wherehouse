whereHouse.controller('customerCtrl', function ($rootScope, $scope, authService, customerService, growlService) {

  authService.isAuthenticated();
  
  this.allCustomers = customerService.getAllCustomers();

  this.deleteCustomer = function (customerId) {
    customerService.deleteCustomer(customerId);
  };

  $scope.cancel = function (item) {
    $rootScope.modal.close();
    growlService.growl('Customer was not added', 'warning');
  };

  $scope.save = function (customer) {
    customerService.addNewCustomer(customer);
    $rootScope.modal.close();
    growlService.growl('Customer was added to your list', 'success');
  };
  
});
