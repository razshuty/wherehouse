whereHouse.controller('addCustomerCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'addCustomerModalContent.html', 'customerCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
});