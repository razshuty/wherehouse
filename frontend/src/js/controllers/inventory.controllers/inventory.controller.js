whereHouse.controller('inventoryCtrl', function ($scope, $rootScope, authService, inventoryService, growlService) {

  authService.isAuthenticated();

  $scope.allInventory = inventoryService.getAllItems();
  $scope.newItem = {};
  $scope.searchText = '';

  $scope.uploadFile = function(event){
    var files = event.target.files;
    $scope.newItem.img = files[0];
  };

  $scope.filterByLabel = function (label) {
    $scope.searchText = label;
  };

  $scope.addItem = function () {
    inventoryService.addItem($scope.newItem);
    $rootScope.modal.close();
    growlService.growl('Item was added to your inventory', 'success');
  };

  $scope.cancel = function (item) {
    $rootScope.modal.close();
    growlService.growl('Item was not added', 'warning');
  };

  $scope.totalItems = $scope.allInventory.list.length;
  $scope.currentPage = 1;
  $scope.itemsPerPage = 2;

  $scope.$watch("currentPage", function() {
    setPagingData($scope.currentPage);
  });

  function setPagingData(page) {
    var pagedData = $scope.allInventory.list.slice(
      (page - 1) * $scope.itemsPerPage,
      page * $scope.itemsPerPage
    );
    $scope.pagedInventory = pagedData;
  }

  this.deleteItem = function (itemId) {
    inventoryService.deleteItem(itemId);
  };

  $scope.queryItem = function (newItem) {
    inventoryService.queryItem(newItem.upc, function (response) {
      var foundItem = response[0];
      if (typeof(foundItem) !== 'undefined' && foundItem !== null ) {
        newItem.title = foundItem.title;
        newItem.description = foundItem.description;
        newItem.img = foundItem.images[0];
        newItem.imgAvailable = foundItem.images.length > 0;
      }
    });
    newItem.$edit = true;
  };
});
