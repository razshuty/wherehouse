whereHouse.controller('addItemCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'addItemModalContent.html', 'inventoryCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
});