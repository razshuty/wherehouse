whereHouse.controller('supplierCtrl', function ($rootScope, $scope, authService, supplierService, growlService) {

  authService.isAuthenticated();
  
  this.allSuppliers = supplierService.getAllSuppliers();

  this.deleteSupplier = function (supplierId) {
    supplierService.deleteSupplier(supplierId);
  };

  $scope.cancel = function (item) {
    $rootScope.modal.close();
    growlService.growl('Supplier was not added', 'warning');
  };

  $scope.save = function (supplier) {
    supplierService.addNewSupplier(supplier);
    $rootScope.modal.close();
    growlService.growl('Supplier was added to your list', 'success');
  };
  
});
