whereHouse.controller('addSupplierCtrl', function ($scope, $rootScope, modalFactory) {

  $scope.open = function (size) {
    modalFactory.modalInstances($rootScope, true, size, true, true, 'addSupplierModalContent.html', 'supplierCtrl');
  };
  
  $scope.ok = function () {
    $rootScope.modal.close();
  };
});