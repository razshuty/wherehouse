whereHouse.factory('modalFactory', ['$uibModal', function($uibModal, $rootScope) {
  //Create Modal
  function modalInstances(rootScope, animation, size, backdrop, keyboard, template, controller, content) {    
    var modalInstance = $uibModal.open({
      animation: animation,
      templateUrl: template,
      controller: controller,      
      size: size,
      backdrop: backdrop,
      keyboard: keyboard
    });

    rootScope.modal = modalInstance;
    rootScope.modalContent = content;

  };

  return {
    modalInstances: modalInstances
  };
  
}]);