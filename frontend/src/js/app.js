var whereHouse = angular.module('whereHouse', [
  'ngAnimate',
  'ngResource',
  'ui.router',
  'ui.bootstrap',
  'angular-loading-bar',
  'oc.lazyLoad',
  'nouislider',
  'ngTable',
  'pascalprecht.translate'
]);
