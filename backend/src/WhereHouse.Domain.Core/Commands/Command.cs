﻿using System;
using WhereHouse.Domain.Core.Events;
using FluentValidation.Results;

namespace WhereHouse.Domain.Core.Commands
{
    public abstract class Command : Message
    {
        public Guid CommandId { get;  }
        public DateTime Timestamp { get; }
        public ValidationResult ValidationResult { get; set; }

        protected Command()
        {
            CommandId = Guid.NewGuid();
            Timestamp = DateTime.Now;
            ValidationResult = new ValidationResult();
        }

        public abstract bool IsValid();
    }
}