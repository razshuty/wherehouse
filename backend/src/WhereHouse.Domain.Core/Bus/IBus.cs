﻿using WhereHouse.Domain.Core.Commands;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Domain.Core.Bus
{
    public interface IBus
    {
        void SendCommand<T>(T theCommand) where T : Command;
        void RaiseEvent<T>(T theEvent) where T : Event;
    }
}