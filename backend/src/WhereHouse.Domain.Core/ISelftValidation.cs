﻿using FluentValidation.Results;

namespace WhereHouse.Domain.Core
{
    public interface ISelftValidation
    {
        ValidationResult ValidationResult { get; }
        bool IsValid();
    }
}