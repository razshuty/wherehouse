﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using System.IO;
using System;

namespace WhereHouse.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "WhereHouse - API";
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

    }
}
