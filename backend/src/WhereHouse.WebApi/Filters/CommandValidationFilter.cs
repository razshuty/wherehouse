﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.WebApi.Filters
{
    public class CommandValidationFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            foreach (var arg in context.ActionArguments)
            {
                if (arg.Value is Command)
                {
                    var command = arg.Value as Command;
                    if (!command.IsValid())
                    {
                        context.Result = new BadRequestResult();
                    }
                }
            }
        }
    }
}