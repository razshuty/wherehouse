﻿using System.IO;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using WhereHouse.Infra.IoC;
using WhereHouse.WebApi.Configurations;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.PlatformAbstractions;
using WhereHouse.Application.AutoMapper;
using WhereHouse.WebApi.Settings;
using WhereHouse.WebApi.Filters;

namespace WhereHouse.WebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            AppSettings = new AppSettings();
            Configuration.Bind(AppSettings);
        }

        public IConfigurationRoot Configuration { get; }
        public AppSettings AppSettings { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<AppSettings>(Configuration);

            services.AddMemoryCache();

            if (AppSettings.ApiApp.RequireHttps)
                services.Configure<MvcOptions>(opt => opt.Filters.Add(new RequireHttpsAttribute()));

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = AppSettings.IdentityServer.Authority;
                    options.RequireHttpsMetadata = AppSettings.IdentityServer.RequireHttpsMetadata;

                    options.ApiName = AppSettings.IdentityServer.ApiName;
                    options.ApiSecret = AppSettings.IdentityServer.ApiSecret;
                });

            services.AddMvcCore(opt =>
                {
                    opt.Filters.Add(new CommandValidationFilter());
                    opt.UseCentralRoutePrefix(new RouteAttribute("api/v{version}"));
                })
                .AddApiExplorer()
                .AddAuthorization()
                .AddJsonFormatters()
                .AddCors();

            AutoMapperConfig.RegisterMappings();

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "WhereHouse",
                    Description = "WhereHouse API surface",
                    Contact = new Contact
                    {
                        Name = "FluffyTeam",
                        Email = "info@wherehouse.de",
                        Url = "https://wherehouse.de"
                    },
                    License = new License {Name = "License", Url = "https://wherehouse.de"}
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "WhereHouse.WebApi.xml");
                s.IncludeXmlComments(xmlPath);
            });

            services.AddMediatR(typeof(Startup));

            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                              IHostingEnvironment env, 
                              ILoggerFactory loggerFactory, 
                              IHttpContextAccessor accessor)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (AppSettings.ApiApp.RequireHttps)
            {
                app.UseRewriter(new RewriteOptions().AddRedirectToHttps());
            }

            app.UseCors(c =>
            {
                c.AllowAnyHeader();
                c.AllowAnyMethod();
                c.AllowAnyOrigin();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(s => s.SwaggerEndpoint("/swagger/v1/swagger.json", "WhereHouse API v1.0"));
            
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Adding dependencies from another layers (isolated from Presentation)
            DependencyInjectorBootStrapper.RegisterServices(services);
        }
    }    
}
