﻿using Microsoft.AspNetCore.Mvc;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using System;
using System.Collections.Generic;
using MediatR;
using WhereHouse.Application.Commands.CompanyCommands;
using WhereHouse.Application.Commands.LocationCommands;

namespace WhereHouse.WebApi.Controllers
{
    [Route("companies")]
    public class CompanyController : ApiController
    {
        private readonly ICompanyAppService _companyAppService;
        private readonly ILocationAppService _locationAppService;

        public CompanyController(ICompanyAppService companyAppService,
                                 ILocationAppService locationAppService)
        {
            _companyAppService = companyAppService;
            _locationAppService = locationAppService;
        }

        /// <summary>
        /// Retrieve company/account list
        /// </summary>
        /// <returns code="200"></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(CompanyViewModel[]),200)]
        public IActionResult Get()
        {
            var companys = _companyAppService.GetAll();
            return Response(companys);
        }

        /// <summary>
        /// Retrieve detail info about a specific company/account
        /// </summary>
        /// <param name="id">Company unique identifier</param>
        /// <returns code="200">Detail info about the company/account</returns>
        /// <returns code="404">Company not found</returns>
        /// <returns code="403">Company exists, but no rights to access</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CompanyViewModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult Get(Guid id)
        {
            var company = _companyAppService.GetById(id);
            if (company == null) return NotFound();
            return Response(company);
        }

        /// <summary>
        /// Create a new company/account under current credential
        /// </summary>
        /// <param name="command">Data to create a new company/account</param>
        /// <returns code="200">The create command was accepted</returns>
        /// <returns code="400">The create command in invalid</returns>
        [HttpPost("")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Post([FromBody]CreateCompanyCommand command)
        {            
            var success = _companyAppService.Create(command);
            if (!success) ValidationResult = _companyAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Update company/account data
        /// </summary>
        /// <param name="id">Company unique identifier</param>
        /// <param name="command">Updated data about company</param>
        /// <returns code="200">The update company was accepted</returns>
        /// <returns code="400">The update commmand is invalid</returns>
        /// <returns code="403">Not enought rights to update the company</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Put(Guid id, [FromBody]UpdateCompanyCommand command)
        {
            if (command.Id != id) return BadRequest();
          var success =  _companyAppService.Update(command);
            if (!success) ValidationResult = _companyAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Remove/deactivate a company/account
        /// </summary>
        /// <param name="command">Command to confirm company removal</param>
        /// <returns code="200">The remove/deactivate command was accepted</returns>
        /// <returns code="400">The remove/deactivate command is invalid</returns>
        /// <returns code="403">Not enought rights to emove/deactivate the company</returns>
        /// <returns code="404">Company not found</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult Delete(DeleteCompanyCommand command)
        {
            var success = _companyAppService.Delete(command);
            if (!success) ValidationResult = _companyAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Retrieve all locations of a company/account
        /// </summary>
        /// <param name="id">Company unique identifier</param>
        /// <returns code="200">The location list is returned</returns>
        /// <returns code="403">No access to company location</returns>
        [HttpGet("{id}/locations")]
        [ProducesResponseType(typeof(LocationViewModel[]), 200)]
        [ProducesResponseType(403)]
        public IEnumerable<LocationViewModel> GetLocations(Guid id)
        {
            var locations = _locationAppService.GetAllFromCompany(id);
            return locations;
        }

        /// <summary>
        /// Add a new location to a company/account
        /// </summary>
        /// <param name="id">Company unique identifier</param>
        /// <param name="command">Data to create a new location</param>
        /// <returns code="200">The add new location command was accepted</returns>
        /// <returns code="400">The add new location command is invalid.</returns>
        /// <returns code="403">Not engouth access to create a new location to this company/account.</returns>
        [HttpPost("{id}/locations")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public IActionResult PostLocations(Guid id, [FromBody] CreateLocationCommand command)
        {
            if (command.CompanyId != id) return BadRequest();

            var success = _locationAppService.Create(command);
            if (!success) ValidationResult = _companyAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Change the ownership of a company/account to another person
        /// </summary>
        /// <param name="id">Company unique identifier</param>
        /// <param name="command">Command data to complete the action</param>
        /// <returns code="200">The command was accepted</returns>
        /// <returns code="400">The command is invalid</returns>
        /// <returns code="403">Not engouth permission to change the ownership</returns>
        [HttpPost("{id}/ChangeOwner")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public IActionResult ChangeOwnership(Guid id, [FromBody] ChangeCompanyOwnershipCommand command)
        {
            if (command.CompanyId != id) return BadRequest();

            var success = _companyAppService.ChangeOwnership(command);
            if (!success) ValidationResult = _companyAppService.ValidationResult;
            return Response();
        }
    }
}