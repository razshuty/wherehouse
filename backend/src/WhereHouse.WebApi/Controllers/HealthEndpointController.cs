﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WhereHouse.WebApi.Controllers
{
    [Route("[controller]")]
    public class HealthEndpointController : ApiController
    {
        public HealthEndpointController()
        {
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get()
        {
            return Ok();
        }
    }
}