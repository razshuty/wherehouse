﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using FluentValidation.Results;

namespace WhereHouse.WebApi.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public abstract class ApiController : ControllerBase
    {
        protected ValidationResult ValidationResult { get; set; }

        protected ApiController()
        {
            ValidationResult = new ValidationResult();
        }

        protected bool IsValidOperation() => ValidationResult.IsValid;

        protected new IActionResult Response(object result = null)
        {
            if (IsValidOperation())
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = ValidationResult.Errors.Select(err => err.ErrorMessage)
            });
        }
    }
}
