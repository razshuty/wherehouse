﻿using Microsoft.AspNetCore.Mvc;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using System;
using MediatR;
using WhereHouse.Application.Commands.CustomerCommands;

namespace WhereHouse.WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : ApiController
    {
        private readonly ICustomerAppService _customerAppService;

        public CustomerController(ICustomerAppService customerAppService)
        {
            _customerAppService = customerAppService;
        }

        /// <summary>
        /// Retrieve customer list
        /// </summary>
        /// <returns code="200"></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(CustomerViewModel[]), 200)]
        public IActionResult Get()
        {
            var customers = _customerAppService.GetAll();
            return Response(customers);
        }

        /// <summary>
        /// Retrieve detail info about a specific customer
        /// </summary>
        /// <param name="id">Customer unique identifier</param>
        /// <returns code="200">Detail info about the customer</returns>
        /// <returns code="404">Customer not found</returns>
        /// <returns code="403">Customer exists, but no rights to access</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerViewModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult Get(Guid id)
        {
            var customer = _customerAppService.GetById(id);
            if (customer == null) return NotFound();
            return Response(customer);
        }

        /// <summary>
        /// Create a new customer under current credential
        /// </summary>
        /// <param name="command">Data to create a new customer</param>
        /// <returns code="200">The create command was accepted</returns>
        /// <returns code="400">The create command in invalid</returns>
        [HttpPost("")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Post([FromBody] CreateCustomerCommand command)
        {
            var success = _customerAppService.Create(command);
            if (!success) ValidationResult = _customerAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Update customer data
        /// </summary>
        /// <param name="id">Customer unique identifier</param>
        /// <param name="command">Updated data about customer</param>
        /// <returns code="200">The update customer was accepted</returns>
        /// <returns code="400">The update commmand is invalid</returns>
        /// <returns code="403">Not enought rights to update the customer</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Put(Guid id, [FromBody] UpdateCustomerCommand command)
        {
            if (command.Id != id) return BadRequest();
            var success = _customerAppService.Update(command);
            if (!success) ValidationResult = _customerAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Remove/deactivate a customer
        /// </summary>
        /// <param name="command">Command to confirm customer removal</param>
        /// <returns code="200">The remove/deactivate command was accepted</returns>
        /// <returns code="400">The remove/deactivate command is invalid</returns>
        /// <returns code="403">Not enought rights to emove/deactivate the customer</returns>
        /// <returns code="404">Customer not found</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult Delete(DeleteCustomerCommand command)
        {
            var success = _customerAppService.Delete(command);
            if (!success) ValidationResult = _customerAppService.ValidationResult;
            return Response();
        }
    }
}