﻿using Microsoft.AspNetCore.Mvc;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using System;
using MediatR;
using WhereHouse.Application.Commands.SupplierCommands;

namespace WhereHouse.WebApi.Controllers
{
    [Route("suppliers")]
    public class SupplierController : ApiController
    {
        private readonly ISupplierAppService _supplierAppService;

        public SupplierController(ISupplierAppService supplierAppService)
            {
            _supplierAppService = supplierAppService;
        }

        /// <summary>
        /// Retrieve Supplier list
        /// </summary>
        /// <returns code="200"></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(SupplierViewModel[]), 200)]
        public IActionResult Get()
        {
            var suppliers = _supplierAppService.GetAll();
            return Response(suppliers);
        }

        /// <summary>
        /// Retrieve detail info about a specific Supplier
        /// </summary>
        /// <param name="id">Supplier unique identifier</param>
        /// <returns code="200">Detail info about the Supplier</returns>
        /// <returns code="404">Supplier not found</returns>
        /// <returns code="403">Supplier exists, but no rights to access</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SupplierViewModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult Get(Guid id)
        {
            var supplier = _supplierAppService.GetById(id);
            if (supplier == null) return NotFound();
            return Response(supplier);
        }

        /// <summary>
        /// Create a new Supplier under current credential
        /// </summary>
        /// <param name="command">Data to create a new Supplier</param>
        /// <returns code="200">The create command was accepted</returns>
        /// <returns code="400">The create command in invalid</returns>
        [HttpPost("")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Post([FromBody] CreateSupplierCommand command)
        {
            var success = _supplierAppService.Create(command);
            if (!success) ValidationResult = _supplierAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Update Supplier data
        /// </summary>
        /// <param name="id">Supplier unique identifier</param>
        /// <param name="command">Updated data about Supplier</param>
        /// <returns code="200">The update Supplier was accepted</returns>
        /// <returns code="400">The update commmand is invalid</returns>
        /// <returns code="403">Not enought rights to update the Supplier</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        //[ProducesResponseType(typeof(DomainNotification[]), 400)]
        public IActionResult Put(Guid id, [FromBody] UpdateSupplierCommand command)
        {
            if (command.Id != id) return BadRequest();
            var success = _supplierAppService.Update(command);
            if (!success) ValidationResult = _supplierAppService.ValidationResult;
            return Response();
        }

        /// <summary>
        /// Remove/deactivate a Supplier
        /// </summary>
        /// <param name="command">Command to confirm Supplier removal</param>
        /// <returns code="200">The remove/deactivate command was accepted</returns>
        /// <returns code="400">The remove/deactivate command is invalid</returns>
        /// <returns code="403">Not enought rights to emove/deactivate the Supplier</returns>
        /// <returns code="404">Supplier not found</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult Delete(DeleteSupplierCommand command)
        {
            var success = _supplierAppService.Delete(command);
            if (!success) ValidationResult = _supplierAppService.ValidationResult;
            return Response();
        }
    }
}