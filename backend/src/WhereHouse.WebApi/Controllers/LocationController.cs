﻿using Microsoft.AspNetCore.Mvc;
using WhereHouse.Application.Interfaces;
using System;
using MediatR;
using WhereHouse.Application.Commands.LocationCommands;

namespace WhereHouse.WebApi.Controllers
{
    [Route("locations")]
    public class LocationController : ApiController
    {
        private readonly ILocationAppService _locationAppService;

        public LocationController(ILocationAppService locationAppService)
        {
            _locationAppService = locationAppService;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var location = _locationAppService.GetById(id);
            if (location == null) return NotFound();
            return Response(location);
        }

        [HttpPut("locations/{id}")]
        public IActionResult Put(int id, [FromBody]UpdateLocationCommand command)
        {
            var success = _locationAppService.Update(command);
            if (!success) ValidationResult = _locationAppService.ValidationResult;
            return Response();
        }

        [HttpDelete("locations/{id}")]
        public IActionResult Delete(RemoveLocationCommand command)
        {
            _locationAppService.Delete(command);
            return Response();
        }
    }
}