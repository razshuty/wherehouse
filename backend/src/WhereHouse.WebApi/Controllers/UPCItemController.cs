﻿using Microsoft.AspNetCore.Mvc;
using WhereHouse.Application.Interfaces;
using System.Threading.Tasks;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.WebApi.Controllers
{
    [Route("upcitem")]
    public class UPCItemController : ApiController
    {
        private readonly IUPCItemAppService _upcItemAppService;

        public UPCItemController(IUPCItemAppService upcItemAppService)
        {
            _upcItemAppService = upcItemAppService;
        }

        /// <summary>
        /// Retrieve detail data about a EAN or UPC code
        /// </summary>
        /// <param name="code">EAN or UPC code</param>
        /// <returns code="200">Returns a UPC detail document</returns>
        [HttpGet("{code}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(UPCItemViewModel), 200)]
        public async Task<IActionResult> Get([FromRoute] string code)
        {
            var items = await _upcItemAppService.Lookup(code);
            return Response(items);
        }
    }
}