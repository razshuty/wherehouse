﻿namespace WhereHouse.WebApi.Settings
{
    public class AppSettings
    {
        public IdentityServerSettings IdentityServer { get; set; } 
        public ApiAppSettings ApiApp { get; set; }
        public ApplicationInsightsSettings ApplicationInsights { get; set; }

        public AppSettings()
        {
            IdentityServer = new IdentityServerSettings();
            ApiApp = new ApiAppSettings();
            ApplicationInsights = new ApplicationInsightsSettings();
        }
    }

    public class IdentityServerSettings
    {
        public string Authority { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public string ApiName { get; set; }
        public string ApiSecret { get; set; }
        public string[] AllowedScopes { get; set; }
    }
    public class ApiAppSettings
    {
        public string SiteUrl { get; set; }
        public bool RequireHttps { get; set; }
        public double UserInfoCacheExpirationSeconds { get; set; }
    }

    public class ApplicationInsightsSettings
    {
        public string IntrumentationKey { get; set; }
    }
}
