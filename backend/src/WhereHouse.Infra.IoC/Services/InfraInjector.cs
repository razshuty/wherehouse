﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using UPCItemDb;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.AspNetIdentity;

namespace WhereHouse.Infra.IoC.Services
{
    public class InfraInjector
    {
        public static void Register(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Infra - Bus
            //services.AddScoped<IBus, InMemoryBus>();

            // Infra - Identity
            services.AddScoped<IUser, AspNetUser>();

            services.AddScoped<UPCItemDBClient>();
        }
    }
}
