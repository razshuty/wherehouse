﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.Services;

namespace WhereHouse.Infra.IoC.Services
{
    public class ApplicationInjector
    {
        public static void Register(IServiceCollection services)
        {
            // Application
            services.AddSingleton(Mapper.Configuration);
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            services.AddScoped<ICompanyAppService, CompanyAppService>();
            services.AddScoped<ILocationAppService, LocationAppService>();
            services.AddScoped<ICustomerAppService, CustomerAppService>();
            services.AddScoped<ISupplierAppService, SupplierAppService>();

            services.AddScoped<IUPCItemAppService, UPCItemAppService>();
        }
    }
}
