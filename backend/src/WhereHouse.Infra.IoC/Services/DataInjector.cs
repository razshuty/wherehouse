﻿using Microsoft.Extensions.DependencyInjection;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;
using WhereHouse.Infra.Data.Repository;
using WhereHouse.Infra.Data.Repository.Documents;
using WhereHouse.Infra.Data.UoW;

namespace WhereHouse.Infra.IoC.Services
{
    public class DataInjector
    {
        public static void Register(IServiceCollection services)
        {
            // Infra - SQL Data
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ISupplierRepository, SupplierRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<WhereHouseContext>();

            // Infra - NoSQL Data
            services.AddScoped<WhereHouseNoSQLContext>();
            services.AddScoped<IUPCItemRepository, UPCItemRepository>();
        }
    }
}
