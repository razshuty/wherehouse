﻿using Microsoft.Extensions.DependencyInjection;
using WhereHouse.Infra.IoC.Services;

namespace WhereHouse.Infra.IoC
{
    public class DependencyInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            ApplicationInjector.Register(services);
            DataInjector.Register(services);
            InfraInjector.Register(services);
        }
    }
}