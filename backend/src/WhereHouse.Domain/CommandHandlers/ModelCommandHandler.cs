﻿using MediatR;
using System;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.ModelCommands;
using WhereHouse.Infra.Bus.Events.ModelEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class ModelCommandHandler : CommandHandler,
        IHandler<CreateModelCommand>,
        IHandler<UpdateModelCommand>,
        IHandler<RemoveModelCommand>
    {
        private readonly IModelRepository _modelRepository;
        private readonly IBus Bus;

        public ModelCommandHandler(IModelRepository modelRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _modelRepository = modelRepository;
            Bus = bus;
        }

        public void Handle(CreateModelCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var Model = new Model(Guid.NewGuid(), message.Name, message.PartNumber);

            if (_modelRepository.GetById(Model.Id) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The already exists!"));
                return;
            }

            _modelRepository.Add(Model);

            if (Commit())
            {
                Bus.RaiseEvent(new ModelCreatedEvent(Model.Id, Model.Name, Model.PartNumber, Model.CreatedDate));
            }
        }

        public void Handle(UpdateModelCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var model = new Model(message.Id, message.Name, message.PartNumber);
            var existingModel = _modelRepository.GetById(model.Id);

            if (existingModel != null)
            {
                if (!existingModel.Equals(model))
                {
                    Bus.RaiseEvent(new DomainNotification(message.MessageType, "The Model Id already been taken."));
                    return;
                }
            }

            _modelRepository.Update(model);

            if (Commit())
            {
                Bus.RaiseEvent(new ModelUpdatedEvent(model.Id, model.Name, model.PartNumber, model.CreatedDate));
            }
        }

        public void Handle(RemoveModelCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _modelRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new ModelRemovedEvent(message.Id));
            }
        }

        public void Dispose()
        {
            _modelRepository.Dispose();
        }
    }
}