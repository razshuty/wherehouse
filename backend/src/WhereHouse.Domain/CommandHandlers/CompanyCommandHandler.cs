﻿using MediatR;
using System;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.CompanyCommands;
using WhereHouse.Infra.Bus.Events.CompanyEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class CompanyCommandHandler : CommandHandler,
        IHandler<CreateCompanyCommand>,
        IHandler<UpdateCompanyCommand>,
        IHandler<RemoveCompanyCommand>,
        IHandler<ChangeCompanyOwnershipCommand>
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IBus Bus;

        public CompanyCommandHandler(ICompanyRepository companyRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) 
            : base(uow, bus, notifications)
        {
            _companyRepository = companyRepository;
            Bus = bus;
        }

        public void Handle(CreateCompanyCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var company = new Company(Guid.NewGuid(), message.Name, message.OwnerId);
            if (!company.IsValid())
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The company is invalid!"));
                return;
            }

            if (_companyRepository.GetById(company.Id) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The company already exists!"));
                return;
            }

            _companyRepository.Add(company);

            if (Commit())
            {
                Bus.RaiseEvent(new CompanyCreatedEvent(company.Id, company.Name, company.CreatedDate));
            }
        }

        public void Handle(UpdateCompanyCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var company = _companyRepository.GetById(message.Id);

            if (company == null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "Company not exists."));
                return;
            }

            if (!company.TryRename(message.Name))
            {
                NotifyValidatorErrors(company);
            }

            _companyRepository.Update(company);

            if (Commit())
            {
                Bus.RaiseEvent(new CompanyUpdatedEvent(company.Id, company.Name, company.CreatedDate));
            }
        }

        public void Handle(RemoveCompanyCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _companyRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new CompanyRemovedEvent(message.Id));
            }
        }

        public void Handle(ChangeCompanyOwnershipCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var company = _companyRepository.GetById(message.CompanyId);
            
            if (!company.TryChangeOwnership(message.NewOwnerId))
            {
                NotifyValidatorErrors(company);
            }

            _companyRepository.Update(company);

            if (Commit())
            {
                Bus.RaiseEvent(new CompanyUpdatedEvent(company.Id, company.Name, company.CreatedDate));
            }
        }

        public void Dispose()
        {
            _companyRepository.Dispose();
        }
    }
}