﻿using MediatR;
using System;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.LocationCommands;
using WhereHouse.Infra.Bus.Events.LocationEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class LocationCommandHandler : CommandHandler,
        IHandler<CreateLocationCommand>,
        IHandler<UpdateLocationCommand>,
        IHandler<RemoveLocationCommand>
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ILocationRepository _locationRepository;
        private readonly IBus Bus;

        public LocationCommandHandler(
            ICompanyRepository companyRepository,
            ILocationRepository locationRepository,
            IUnitOfWork uow,
            IBus bus,
            INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _locationRepository = locationRepository;
            Bus = bus;
            _companyRepository = companyRepository;
        }

        public void Handle(CreateLocationCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var location = new Location(Guid.NewGuid(), message.Name, message.CompanyId);
            if (!location.IsValid())
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "Invalid location!"));
                return;
            }

            var companyExists = _companyRepository.Exists(location.CompanyId);
            if (!companyExists)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "Invalid company!"));
                return;
            }

            _locationRepository.Add(location);

            if (Commit())
            {
                Bus.RaiseEvent(new LocationCreatedEvent(location.Id, location.Name, "", location.CreatedDate));
            }
        }

        public void Handle(UpdateLocationCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var location = _locationRepository.GetById(message.Id);

            if (location == null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "Company not exists."));
                return;
            }

            if (!location.TryChangeName(message.Name))
            {
                NotifyValidatorErrors(location);
            }

            _locationRepository.Update(location);

            if (Commit())
            {
                Bus.RaiseEvent(new LocationUpdatedEvent(location.Id, location.Name, location.CreatedDate));
            }
        }

        public void Handle(RemoveLocationCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _locationRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new LocationRemovedEvent(message.Id));
            }
        }

        public void Dispose()
        {
            _locationRepository.Dispose();
        }
    }
}