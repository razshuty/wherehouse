﻿using MediatR;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.CustomerCommands;
using WhereHouse.Infra.Bus.Events.CustomerEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class CustomerCommandHandler : CommandHandler,
        IHandler<CreateCustomerCommand>,
        IHandler<UpdateCustomerCommand>,
        IHandler<DeleteCustomerCommand>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IBus Bus;

        public CustomerCommandHandler(ICustomerRepository customerRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) 
            : base(uow, bus, notifications)
        {
            _customerRepository = customerRepository;
            Bus = bus;
        }

        public void Handle(CreateCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var customer = new Customer(message.FirstName, message.LastName, message.Email);
            if (!customer.IsValid())
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The Customer is invalid!"));
                return;
            }

            if (_customerRepository.GetById(customer.Id) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The Customer already exists!"));
                return;
            }

            _customerRepository.Add(customer);

            if (Commit())
            {
                Bus.RaiseEvent(new CustomerCreatedEvent
                (customer.Id, customer.FullName.FirstName, customer.FullName.LastName, customer.Contact.Data, customer.CreatedDate));
            }
        }

        public void Handle(UpdateCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var customer = _customerRepository.GetById(message.Id);

            if (customer == null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "Customer not exists."));
                return;
            }

            //if (!customer.TryRename(message.Name))
            //{
            //    NotifyValidatorErrors(customer);
            //}

            _customerRepository.Update(customer);

            if (Commit())
            {
                Bus.RaiseEvent(new CustomerUpdatedEvent
                    (customer.Id, customer.FullName.FirstName, customer.FullName.LastName, customer.Contact.Data, customer.CreatedDate));
            }
        }

        public void Handle(DeleteCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _customerRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new CustomerRemovedEvent(message.Id));
            }
        }

        public void Dispose()
        {
            _customerRepository.Dispose();
        }
    }
}