﻿using MediatR;
using System;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.ItemCommands;
using WhereHouse.Infra.Bus.Events.ItemEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class ItemCommandHandler : CommandHandler,
        IHandler<CreateItemCommand>,
        IHandler<UpdateItemCommand>,
        IHandler<DeleteItemCommand>
    {
        private readonly IItemRepository _itemRepository;
        private readonly IBus Bus;

        public ItemCommandHandler(IItemRepository itemRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _itemRepository = itemRepository;
            Bus = bus;
        }

        public void Handle(CreateItemCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var item = new Item(Guid.NewGuid(), message.Amount);

            if (_itemRepository.GetById(item.Id) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The already exists!"));
                return;
            }

            _itemRepository.Add(item);

            if (Commit())
            {
                Bus.RaiseEvent(new ItemCreatedEvent(item.Id, "model", item.Amount, item.CreatedDate));
            }
        }

        public void Handle(UpdateItemCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var item = new Item(message.Id, message.Amount);
            var existingItem = _itemRepository.GetById(item.Id);

            if (existingItem != null)
            {
                if (!existingItem.Equals(item))
                {
                    Bus.RaiseEvent(new DomainNotification(message.MessageType, "The item Id already been taken."));
                    return;
                }
            }

            _itemRepository.Update(item);

            if (Commit())
            {
                Bus.RaiseEvent(new ItemUpdatedEvent(item.Id, "Model", item.Amount, item.CreatedDate));
            }
        }

        public void Handle(DeleteItemCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _itemRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new ItemRemovedEvent(message.Id));
            }
        }

        public void Dispose()
        {
            _itemRepository.Dispose();
        }
    }
}