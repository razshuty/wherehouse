﻿using MediatR;
using System;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.TrackCommands;
using WhereHouse.Infra.Bus.Events.TrackEvents;

namespace WhereHouse.Domain.CommandHandlers
{
    public class TrackCommandHandler : CommandHandler,
        IHandler<CreateTrackCommand>,
        IHandler<RemoveTrackCommand>
    {
        private readonly ITrackRepository _trackRepository;
        private readonly IBus Bus;

        public TrackCommandHandler(ITrackRepository trackRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) : base(uow, bus, notifications)
        {
            _trackRepository = trackRepository;
            Bus = bus;
        }

        public void Handle(CreateTrackCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var track = new Track(Guid.NewGuid(), null, null);

            if (_trackRepository.GetById(track.Id) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The location already exists!"));
                return;
            }

            _trackRepository.Add(track);

            if (Commit())
            {
                Bus.RaiseEvent(new TrackCreatedEvent(track.Id, "", "", track.CreatedDate));
            }
        }

        public void Handle(RemoveTrackCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            _trackRepository.Remove(message.Id);

            if (Commit())
            {
                Bus.RaiseEvent(new TrackRemovedEvent(message.Id));
            }
        }

        public void Dispose()
        {
            _trackRepository.Dispose();
        }
    }
}