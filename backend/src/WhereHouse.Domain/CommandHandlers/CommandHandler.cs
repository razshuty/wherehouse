﻿using MediatR;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Commands;
using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Domain.CommandHandlers
{
    public class CommandHandler
    {
        private readonly IUnitOfWork _uow;
        private readonly IBus _bus;
        private readonly DomainNotificationHandler _notifications;

        public CommandHandler(IUnitOfWork uow, IBus bus, INotificationHandler<DomainNotification> notifications)
        {
            _uow = uow;
            _notifications = notifications as DomainNotificationHandler;
            _bus = bus;
        }

        protected void NotifyValidatorErrors(Command message)
        {
            foreach (var error in message.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(message.MessageType, error.ErrorMessage));
            }
        }
        protected void NotifyValidatorErrors(Entity entity)
        {
            foreach (var error in entity.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(entity.GetType().Name, error.ErrorMessage));
            }
        }

        public bool Commit()
        {
            if (_notifications.HasNotifications()) return false;
            var commandResponse = _uow.Commit();
            if (commandResponse.Success) return true;

            _bus.RaiseEvent(new DomainNotification("Commit", "We had a problem during saving your data."));
            return false;
        }
    }
}