﻿using MediatR;
using WhereHouse.Domain.Core.Bus;
using WhereHouse.Domain.Core.Events;
using WhereHouse.Domain.Core.Notifications;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Bus.Commands.UPCItemCommands;

namespace WhereHouse.Domain.CommandHandlers
{
    public class UPCItemCommandHandler : CommandHandler,
        IHandler<CreateUPCItemCommand>
    {
        private readonly IUPCItemRepository _upcItemRepository;
        private readonly IBus Bus;

        public UPCItemCommandHandler(IUPCItemRepository upcItemRepository,
                                      IUnitOfWork uow,
                                      IBus bus,
                                      INotificationHandler<DomainNotification> notifications) 
            : base(uow, bus, notifications)
        {
            _upcItemRepository = upcItemRepository;
            Bus = bus;
        }

        public void Handle(CreateUPCItemCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidatorErrors(message);
                return;
            }

            var upcItem = new UPCItem(message.EAN);
            if (!upcItem.IsValid())
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The UPC Item is invalid!"));
                return;
            }

            if (_upcItemRepository.Find(item=>item.EAN == upcItem.EAN) != null)
            {
                Bus.RaiseEvent(new DomainNotification(message.MessageType, "The UPC Item already exists!"));
                return;
            }

            _upcItemRepository.Add(upcItem);

            Commit();
        }

        public void Dispose()
        {
            _upcItemRepository.Dispose();
        }
    }
}