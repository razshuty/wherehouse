﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Domain.Commands.TrackCommands
{
    public abstract class TrackCommand : Command
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}