﻿using System;
using WhereHouse.Domain.Validators.TrackValidators;

namespace WhereHouse.Domain.Commands.TrackCommands
{
    public class CreateTrackCommand : TrackCommand
    {
        public CreateTrackCommand(string name, string type)
        {
            Id = Guid.NewGuid();
            Name = name;
            Type = type;
            CreatedDate = DateTime.UtcNow;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateTrackCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}