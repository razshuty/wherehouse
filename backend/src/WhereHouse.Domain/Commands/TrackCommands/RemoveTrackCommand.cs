﻿using System;
using WhereHouse.Domain.Validators.TrackValidators;

namespace WhereHouse.Domain.Commands.TrackCommands
{
    public class RemoveTrackCommand : TrackCommand
    {
        public RemoveTrackCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveTrackCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}