﻿using System;
using WhereHouse.Domain.Validators.LocationValidators;
using WhereHouse.Infra.CrossCutting.Bus.Validators.LocationValidators;

namespace WhereHouse.Domain.Commands.LocationCommands
{
    public class UpdateLocationCommand : LocationCommand
    {
        public UpdateLocationCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}