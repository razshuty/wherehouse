﻿using System;
using WhereHouse.Infra.CrossCutting.Bus.Validators.LocationValidators;

namespace WhereHouse.Domain.Commands.LocationCommands
{
    public class RemoveLocationCommand : LocationCommand
    {
        public RemoveLocationCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}