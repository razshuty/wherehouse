﻿using System;
using WhereHouse.Infra.CrossCutting.Bus.Validators.LocationValidators;

namespace WhereHouse.Domain.Commands.LocationCommands
{
    public class CreateLocationCommand : LocationCommand
    {
        public CreateLocationCommand(Guid companyId, string name)
        {
            CompanyId = companyId;
            Id = Guid.NewGuid();
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}