﻿using System;
using WhereHouse.Domain.Validators.ModelValidators;

namespace WhereHouse.Domain.Commands.ModelCommands
{
    public class RemoveModelCommand : ModelCommand
    {
        public RemoveModelCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveModelCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}