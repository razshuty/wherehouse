﻿using System;
using WhereHouse.Domain.Validators.ModelValidators;

namespace WhereHouse.Domain.Commands.ModelCommands
{
    public class CreateModelCommand : ModelCommand
    {
        public CreateModelCommand(string name, string partNumber)
        {
            Id = Guid.NewGuid();
            Name = name;
            PartNumber = partNumber;
            CreatedDate = DateTime.UtcNow;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateModelCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}