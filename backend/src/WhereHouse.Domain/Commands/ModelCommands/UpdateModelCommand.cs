﻿using System;
using WhereHouse.Domain.Validators.ModelValidators;

namespace WhereHouse.Domain.Commands.ModelCommands
{
    public class UpdateModelCommand : ModelCommand
    {
        public UpdateModelCommand(Guid id, string name, string partNumber, DateTime createdDate)
        {
            Id = id;
            Name = name;
            PartNumber = partNumber;
            CreatedDate = createdDate;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateModelCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}