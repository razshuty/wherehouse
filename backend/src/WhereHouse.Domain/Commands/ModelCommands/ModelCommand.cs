﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Domain.Commands.ModelCommands
{
    public abstract class ModelCommand : Command
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PartNumber { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}