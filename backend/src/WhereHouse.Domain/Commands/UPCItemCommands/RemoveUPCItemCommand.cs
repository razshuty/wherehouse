﻿using System;
using WhereHouse.Infra.CrossCutting.Bus.Validators.UPCItemValidators;

namespace WhereHouse.Domain.Commands.UPCItemCommands
{
    public class RemoveUPCItemCommand : UPCItemCommand
    {
        public RemoveUPCItemCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveUPCItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}