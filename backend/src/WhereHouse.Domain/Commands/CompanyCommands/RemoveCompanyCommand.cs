﻿using System;
using WhereHouse.Infra.CrossCutting.Bus.Validators.CompanyValidators;

namespace WhereHouse.Domain.Commands.CompanyCommands
{
    public class RemoveCompanyCommand : CompanyCommand
    {
        public RemoveCompanyCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}