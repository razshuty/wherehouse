﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Domain.Commands.CompanyCommands
{
    public abstract class CompanyCommand : Command
    {
        public Guid OwnerId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}