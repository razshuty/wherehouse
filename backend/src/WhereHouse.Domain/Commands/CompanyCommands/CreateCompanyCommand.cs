﻿using System;
using WhereHouse.Infra.CrossCutting.Bus.Validators.CompanyValidators;

namespace WhereHouse.Domain.Commands.CompanyCommands
{
    public class CreateCompanyCommand : CompanyCommand
    {
        public CreateCompanyCommand(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}