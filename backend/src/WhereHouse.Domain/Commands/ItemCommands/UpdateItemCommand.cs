﻿using System;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Validators.ItemValidators;

namespace WhereHouse.Domain.Commands.ItemCommands
{
    public class UpdateItemCommand : ItemCommand
    {
        public UpdateItemCommand(Guid id, Model model, decimal amount, DateTime createdDate)
        {
            Id = id;
            Model = model;
            Amount = amount;
            CreatedDate = createdDate;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}