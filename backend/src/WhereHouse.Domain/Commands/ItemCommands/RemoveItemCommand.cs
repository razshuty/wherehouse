﻿using System;
using WhereHouse.Domain.Validators.ItemValidators;

namespace WhereHouse.Domain.Commands.ItemCommands
{
    public class RemoveItemCommand : ItemCommand
    {
        public RemoveItemCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}