﻿using System;
using WhereHouse.Domain.Core.Commands;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Commands.ItemCommands
{
    public abstract class ItemCommand : Command
    {
        public Guid Id { get; set; }

        public Model Model { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}