﻿using System;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Validators.ItemValidators;

namespace WhereHouse.Domain.Commands.ItemCommands
{
    public class CreateItemCommand : ItemCommand
    {
        public CreateItemCommand(string name, Model model, decimal amount)
        {
            Id = Guid.NewGuid();
            Model = model;
            Amount = amount;
            CreatedDate = DateTime.UtcNow;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}