﻿using WhereHouse.Domain.Core.Models;

namespace WhereHouse.Domain.Entities
{
    public class UPCItem : Entity
    {
        protected UPCItem()
        {
        }

        public UPCItem(string ean)
        {
            EAN = ean;
        }

        public string[] Images { get; protected set; }
        public string Currency { get; protected set; }
        public string Weight { get; protected set; }
        public string Dimension { get; protected set; }
        public string Size { get; protected set; }
        public string Color { get; protected set; }
        public string Brand { get; protected set; }
        public string Description { get; protected set; }
        public string ASIN { get; protected set; }
        public string GTIN { get; protected set; }
        public string UPC { get; protected set; }
        public string Title { get; protected set; }
        public string EAN { get; protected set; }
        public string Model { get; protected set; }

        public override bool IsValid() => true;
    }
}