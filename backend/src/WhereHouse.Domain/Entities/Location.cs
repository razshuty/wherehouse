﻿using System;
using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Validators.LocationValidators;

namespace WhereHouse.Domain.Entities
{
    public class Location : Entity
    {
        public Location(Guid id, string name, Guid companyId)
        {
            CompanyId = companyId;
            Id = id;
            Name = name;
        }
        protected Location() { }

        public string Name { get; protected set; }

        public Guid CompanyId { get; set; }

        public override bool IsValid()
        {
            ValidationResult = new LocationSelfValidator().Validate(this);
            return ValidationResult.IsValid;
        }

        public bool TryChangeName(string name)
        {
            ValidationResult = new LocationCanChangeNameValidator().Validate(this);
            if (ValidationResult.IsValid)
            {
                Name = name;
            }

            return ValidationResult.IsValid;
        }

        public bool CanBeDelete()
        {
            //todo: implement LocationCanBeDeletedValidator
            return true;
        }
    }
}