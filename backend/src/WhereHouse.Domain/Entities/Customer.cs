﻿using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Validators.CustomerValidators;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Entities
{
    public class Customer : Entity
    {
        public FullName FullName { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }

        protected Customer() { }

        public Customer(string firstName, string lastName, string email)
        {
            FullName = new FullName {FirstName = firstName, LastName = lastName};
            Contact = new Contact {Type = ContactType.Email, Data = email};
            Address = new Address();
        }

        public override bool IsValid()
        {
            ValidationResult = new CustomerValidator().Validate(this);
            return ValidationResult.IsValid;
        }

        public bool CanBeDelete()
        {
            ValidationResult = new CustomerCanBeRemovedValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
