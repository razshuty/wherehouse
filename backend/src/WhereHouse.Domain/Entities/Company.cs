﻿using System;
using System.Collections.Generic;
using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Validators.CompanyValidators;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Entities
{
    public class Company : Entity
    {
        public string Name { get; set; }
        public Guid OwnerId { get; set; }
        public Address Address { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SupportEmail { get; set; }
        public string WebSite { get; set; }
        public string LogoImgUrl { get; set; }

        public IEnumerable<Location> Locations => CompanyLocations;
        protected ICollection<Location> CompanyLocations { get; set; }

        protected Company()
        {
        }

        public Company(Guid id, string name, Guid ownerId)
        {
            Id = id;
            Name = name;
            OwnerId = ownerId;
        }

        public override bool IsValid()
        {
            ValidationResult = new CompanySelfValidator().Validate(this);
            return ValidationResult.IsValid;
        }

        public bool TryRename(string newName)
        {
            ValidationResult = new CompanyCanChangeNameValidator().Validate(this);
            if (!ValidationResult.IsValid) return false;

            Name = newName;

            return ValidationResult.IsValid;
        }

        public bool AddLocation(Location location)
        {
            ValidationResult = new CompanyCanAddLocationValidator().Validate(this);
            if (!ValidationResult.IsValid) return false;

            CompanyLocations.Add(location);

            return ValidationResult.IsValid;
        }

        public bool RemoveLocation(Location location)
        {
            ValidationResult = new CompanyCanRemoveLocationValidator().Validate(this);
            if (!ValidationResult.IsValid) return false;

            CompanyLocations.Remove(location);

            return ValidationResult.IsValid;
        }

        public bool TryChangeOwnership(Guid newOwnerId)
        {
            ValidationResult = new CompanyCanChangeOwnership().Validate(this);
            if (!ValidationResult.IsValid) return false;

            OwnerId = newOwnerId;

            return ValidationResult.IsValid;
        }
    }
}