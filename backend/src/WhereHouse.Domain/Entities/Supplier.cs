﻿using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Validators.SupplierValidatores;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Entities
{
    public class Supplier : Entity
    {
        public string Name { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }

        public bool TryRename(string newName)
        {
            ValidationResult = new SupplierCanBeRenamedValidator().Validate(this);
            if (ValidationResult.IsValid)
            {
                Name = newName;
            }
            return ValidationResult.IsValid;
        }

        public override bool IsValid()
        {
            ValidationResult = new SupplierValidator().Validate(this);
            return ValidationResult.IsValid;
        }

        public bool CanBeDelete()
        {
            ValidationResult = new SupplierCanBeRemovedValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
