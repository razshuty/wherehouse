﻿using WhereHouse.Domain.Core;

namespace WhereHouse.Domain.Extentions
{
    public static class SelfValidationExtentions
    {
        public static bool IsValid(this ISelftValidation prop)
        {
            return prop.IsValid();
        }
    }
}