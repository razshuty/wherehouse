﻿using System;
using FluentValidation;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Validators.LocationValidators
{
    public class LocationValidator : AbstractValidator<Location>
    {
        public LocationValidator()
        {
            ValidateId();
            ValidateCompanyId();
            ValidateName();
        }

        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateCompanyId()
        {
            RuleFor(c => c.CompanyId)
                .NotEqual(Guid.Empty).WithMessage("Invalid account");
        }
    }
}