﻿namespace WhereHouse.Domain.Validators.LocationValidators
{
    public class LocationSelfValidator : LocationValidator
    {
        public LocationSelfValidator()
        {
            ValidateId();
            ValidateCompanyId();
            ValidateName();
        }        
    }
}