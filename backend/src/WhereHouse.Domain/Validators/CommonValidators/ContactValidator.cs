﻿using FluentValidation;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Validators.CommonValidators
{
    public class ContactValidator : AbstractValidator<Contact>
    {
        public ContactValidator()
        {
            RuleFor(contact => contact.Data)
                .NotEmpty().WithMessage("Contact data cannot be empty");
        }
    }
}