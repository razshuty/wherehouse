﻿using FluentValidation;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Validators.CommonValidators
{
    public class PhoneNumberValidator : AbstractValidator<PhoneNumber>
    {
        public PhoneNumberValidator()
        {
            RuleFor(phoneNumber => phoneNumber.CountryCode)
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(999);

            RuleFor(phoneNumber => phoneNumber.AreaCode)
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(999);

            RuleFor(phoneNumber => phoneNumber.Number)
                .GreaterThan(100);
        }
    }
}