﻿using FluentValidation;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Validators.CommonValidators
{
    public class AddressValidator : AbstractValidator<Address>
    {
        public AddressValidator()
        {
            RuleFor(address => address.Street)
                .NotEmpty().WithMessage("Street name cannot be empty");

            RuleFor(address=>address.Number)
                .NotEmpty().WithMessage("Number cannot be empty");

            RuleFor(address => address.PostalCode)
                .NotEmpty().WithMessage("Postal code cannot be empty");

            RuleFor(address => address.City)
                .NotEmpty().WithMessage("City cannot be empty");

            RuleFor(address => address.Province)
                .NotEmpty().WithMessage("Province cannot be empty");

            RuleFor(address => address.Country)
                .NotEmpty().WithMessage("Country cannot be empty");
        }
    }
}