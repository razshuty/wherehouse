﻿using FluentValidation;
using WhereHouse.Domain.ValueObjects;

namespace WhereHouse.Domain.Validators.CommonValidators
{
    public class FullNameValidator : AbstractValidator<FullName>
    {
        public FullNameValidator()
        {
            RuleFor(fullname => fullname.FirstName)
                .NotEmpty().WithMessage("First name cannot be empty");

            RuleFor(fullname => fullname.LastName)
                .NotEmpty().WithMessage("Last name cannot be empty");
        }
    }
}