﻿using System;
using FluentValidation;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Extentions;

namespace WhereHouse.Domain.Validators.CustomerValidators
{
    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            ValidateId();
            ValidateName();
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateName()
        {
            RuleFor(c => c.FullName)
                .Must(SelfValidationExtentions.IsValid).WithMessage("Invalid name");
        }

        protected void ValidateContacts()
        {
            RuleFor(c => c.Contact)
                .Must(SelfValidationExtentions.IsValid).WithMessage("Invalid contact");
        }

        protected void ValidateAddress()
        {
            RuleFor(c => c.Address)
                .Must(SelfValidationExtentions.IsValid).WithMessage("Invalid address");
        }
    }
}