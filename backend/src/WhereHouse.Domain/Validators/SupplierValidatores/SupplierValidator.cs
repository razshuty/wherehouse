﻿using System;
using FluentValidation;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Extentions;

namespace WhereHouse.Domain.Validators.SupplierValidatores
{
    public class SupplierValidator : AbstractValidator<Supplier>
    {
        public SupplierValidator()
        {
            ValidateId();
            ValidateName();
            ValidateContact();
            ValidateAddress();
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Invalid name");
        }

        protected void ValidateContact()
        {
            RuleFor(c => c.Contact)
                .Must(SelfValidationExtentions.IsValid).WithMessage("Invalid Contact");
        }
        protected void ValidateAddress()
        {
            RuleFor(c => c.Address)
                .Must(SelfValidationExtentions.IsValid).WithMessage("Invalid Address");
        }

    }
}