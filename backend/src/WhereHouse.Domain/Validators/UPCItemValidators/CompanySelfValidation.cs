﻿namespace WhereHouse.Domain.Validators.UPCItemValidators
{
    public class CompanySelfValidator : CompanyValidator
    {
        public CompanySelfValidator()
        {
            ValidateId();
            ValidateOwnerId();
            ValidateName();
        }        
    }
}