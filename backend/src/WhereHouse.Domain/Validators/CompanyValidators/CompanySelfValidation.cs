﻿namespace WhereHouse.Domain.Validators.CompanyValidators
{
    public class CompanySelfValidator : CompanyValidator
    {
        public CompanySelfValidator()
        {
            ValidateId();
            ValidateOwnerId();
            ValidateName();
        }        
    }
}