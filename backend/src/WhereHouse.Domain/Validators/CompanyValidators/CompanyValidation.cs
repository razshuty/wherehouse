﻿using System;
using System.Linq;
using FluentValidation;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Validators.CompanyValidators
{
    public class CompanyValidator : AbstractValidator<Company>
    {
        public CompanyValidator()
        {
            ValidateId();
            ValidateOwnerId();
            ValidateName();
        }

        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateOwnerId()
        {
            RuleFor(c => c.OwnerId)
                .NotEqual(Guid.Empty).WithMessage("Invalid owner");
        }

        protected void ValidateLocations()
        {
            RuleFor(c => c.Locations.All(l => l.IsValid()))
                .NotEqual(true).WithMessage("One or more location is invalid");
        }
    }
}