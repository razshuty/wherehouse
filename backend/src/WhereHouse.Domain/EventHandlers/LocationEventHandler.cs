﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.LocationEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class LocationEventHandler :
        IHandler<LocationCreatedEvent>,
        IHandler<LocationUpdatedEvent>,
        IHandler<LocationRemovedEvent>
    {
        public void Handle(LocationUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(LocationCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(LocationRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}