﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.CustomerEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class CustomerEventHandler :
        IHandler<CustomerCreatedEvent>,
        IHandler<CustomerUpdatedEvent>,
        IHandler<CustomerRemovedEvent>
    {
        public void Handle(CustomerUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(CustomerCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(CustomerRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}