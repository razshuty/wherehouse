﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.TrackEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class TrackEventHandler :
        IHandler<TrackCreatedEvent>,
        IHandler<TrackUpdatedEvent>,
        IHandler<TrackRemovedEvent>
    {
        public void Handle(TrackUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(TrackCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(TrackRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}