﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.ModelEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class ModelEventHandler :
        IHandler<ModelCreatedEvent>,
        IHandler<ModelUpdatedEvent>,
        IHandler<ModelRemovedEvent>
    {
        public void Handle(ModelUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(ModelCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(ModelRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}