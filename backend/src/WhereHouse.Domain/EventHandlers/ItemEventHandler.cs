﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.ItemEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class ItemEventHandler :
        IHandler<ItemCreatedEvent>,
        IHandler<ItemUpdatedEvent>,
        IHandler<ItemRemovedEvent>
    {
        public void Handle(ItemUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(ItemCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(ItemRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}