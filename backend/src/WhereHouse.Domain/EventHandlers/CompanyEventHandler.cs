﻿using WhereHouse.Domain.Core.Events;
using WhereHouse.Infra.Bus.Events.CompanyEvents;

namespace WhereHouse.Domain.EventHandlers
{
    public class CompanyEventHandler :
        IHandler<CompanyCreatedEvent>,
        IHandler<CompanyUpdatedEvent>,
        IHandler<CompanyRemovedEvent>
    {
        public void Handle(CompanyUpdatedEvent message)
        {
            // Send some notification e-mail
        }

        public void Handle(CompanyCreatedEvent message)
        {
            // logic here when item is created in the system? (email?)
        }

        public void Handle(CompanyRemovedEvent message)
        {
            // Send some see you soon e-mail
        }
    }
}