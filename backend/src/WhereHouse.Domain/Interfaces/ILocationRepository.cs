﻿using System;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Interfaces
{
    public interface ILocationRepository : IRepository<Location>
    {
        Location GetByItemId(Guid id);
    }
}