﻿using System;

namespace WhereHouse.Domain.Interfaces
{
    public interface IUser
    {
        Guid Id { get; }
        string Name { get; }
        bool IsAuthenticated();
    }
}
