﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        CommandResponse Commit();
    }
}
