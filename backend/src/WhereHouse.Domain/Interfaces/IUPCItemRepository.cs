﻿using System.Collections.Generic;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Interfaces
{
    public interface IUPCItemRepository : IRepository<UPCItem>
    {
        IEnumerable<UPCItem> Search(string keyword);
    }
}