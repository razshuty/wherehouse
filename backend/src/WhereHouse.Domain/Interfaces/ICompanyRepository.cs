﻿using System;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Interfaces
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}