﻿using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}