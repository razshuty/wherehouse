﻿using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Interfaces
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
    }
}