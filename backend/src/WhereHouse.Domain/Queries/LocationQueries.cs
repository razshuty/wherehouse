﻿using System;
using System.Linq.Expressions;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Domain.Queries
{
    public class LocationQueries
    {
        public static Expression<Func<Location, bool>> FindByCompanyId(Guid companyId)
        {
            return location => location.CompanyId == companyId;
        }
    }
}