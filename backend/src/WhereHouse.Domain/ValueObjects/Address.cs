﻿using FluentValidation.Results;
using WhereHouse.Domain.Core;
using WhereHouse.Domain.Validators.CommonValidators;

namespace WhereHouse.Domain.ValueObjects
{
    public class Address : ISelftValidation
    {
        public string Street { get; protected set; }
        public string Number { get; protected set; }
        public string PostalCode { get; protected set; }
        public string City { get; protected set; }
        public string Province { get; protected set; }
        public string Country { get; protected set; }
        
        public ValidationResult ValidationResult { get; protected set; }

        public bool IsValid()
        {
            ValidationResult = new AddressValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}