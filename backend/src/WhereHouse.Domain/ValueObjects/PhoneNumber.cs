﻿using FluentValidation.Results;
using WhereHouse.Domain.Core;
using WhereHouse.Domain.Validators.CommonValidators;

namespace WhereHouse.Domain.ValueObjects
{
    public class PhoneNumber : ISelftValidation
    {
        public int? CountryCode { get; protected set; }
        public int? AreaCode { get; protected set; }
        public int? Number { get; protected set; }
        public int? Extention { get; protected set; }

        protected PhoneNumber() { }

        public PhoneNumber(int countryCode, int areaCode, int number, int extention)
        {
            CountryCode = countryCode;
            AreaCode = areaCode;
            Number = number;
            Extention = extention;
        }

        public ValidationResult ValidationResult { get; protected set; }

        public bool IsValid()
        {
            ValidationResult = new PhoneNumberValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}