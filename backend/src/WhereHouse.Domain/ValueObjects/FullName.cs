﻿using FluentValidation.Results;
using WhereHouse.Domain.Core;
using WhereHouse.Domain.Validators.CommonValidators;

namespace WhereHouse.Domain.ValueObjects
{
    public class FullName : ISelftValidation
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ValidationResult ValidationResult { get; protected set; }

        public bool IsValid()
        {
            ValidationResult = new FullNameValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
