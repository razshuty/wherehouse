﻿using System;
using FluentValidation.Results;
using WhereHouse.Domain.Core;
using WhereHouse.Domain.Validators.CommonValidators;

namespace WhereHouse.Domain.ValueObjects
{
    public class Contact : ISelftValidation
    {
        public ContactType Type { get; set; }
        public string Data { get; set; }
        public ValidationResult ValidationResult { get; protected set; }
        public bool IsValid()
        {
            ValidationResult = new ContactValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }

    [Flags]
    public enum ContactType
    {
        Email,
        Phone
    }
}
