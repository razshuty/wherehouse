﻿using System.Threading.Tasks;

namespace WhereHouse.IdentityServer.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
