﻿using System.Threading.Tasks;

namespace WhereHouse.IdentityServer.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
