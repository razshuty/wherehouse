﻿using System;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Repository
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(WhereHouseContext context)
            :base(context)
        {

        }
    }
}
