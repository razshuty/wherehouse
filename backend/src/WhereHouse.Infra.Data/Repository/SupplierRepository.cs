﻿using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Repository
{
    public class SupplierRepository : Repository<Supplier>, ISupplierRepository
    {
        public SupplierRepository(WhereHouseContext context)
            :base(context)
        {

        }
    }
}
