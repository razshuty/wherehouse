﻿using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(WhereHouseContext context)
            :base(context)
        {

        }
    }
}
