﻿using System.Collections.Generic;
using MongoDB.Driver;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Repository.Documents
{
    public class UPCItemRepository :  DocumentRepository<UPCItem>, IUPCItemRepository
    {
        public UPCItemRepository(WhereHouseNoSQLContext context) : base(context.Caching, nameof(UPCItem))
        {
        }

        public IEnumerable<UPCItem> Search(string keyword)
        {
            var filter = FilterBuilder.Text(keyword, new TextSearchOptions {CaseSensitive = false, DiacriticSensitive = false});
            var items = Collection.FindSync(filter).ToList();
            return items;
        }
    }
}