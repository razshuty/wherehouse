﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using WhereHouse.Domain.Core.Models;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Infra.Data.Repository.Documents
{
    public class DocumentRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected IMongoCollection<TEntity> Collection;
        protected FilterDefinitionBuilder<TEntity> FilterBuilder;
        protected UpdateDefinitionBuilder<TEntity> UpdateBuilder;

        public DocumentRepository(IMongoDatabase database, string collectionName)
        {
            Collection = database.GetCollection<TEntity>(collectionName);
            FilterBuilder = new FilterDefinitionBuilder<TEntity>();
            UpdateBuilder = new UpdateDefinitionBuilder<TEntity>();
        }

        public virtual void Add(TEntity document)
        {
            Collection.InsertOne(document);
        }

        public virtual TEntity GetById(Guid id)
        {
            var filter = FilterBuilder.Eq(doc => doc.Id, id);
            return Collection.FindSync(filter).FirstOrDefault();
        }

        public virtual bool Exists(Guid id)
        {
            var filter = FilterBuilder.Eq(doc => doc.Id, id);
            return Collection.FindSync(filter).Any();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            var filter = FilterBuilder.Empty;
            return Collection.FindSync(filter).ToList();
        }

        public virtual void Update(TEntity document)
        {
            var filter = FilterBuilder.Eq(doc => doc.Id, document.Id);
            Collection.ReplaceOne(filter, document);
        }

        public virtual void Remove(Guid id)
        {
            var filter = FilterBuilder.Eq(doc => doc.Id, id);
            Collection.DeleteOne(filter);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Collection.AsQueryable().Where(predicate).ToList();
        }

        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
