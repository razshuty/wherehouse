﻿using System;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Repository
{
    public class LocationRepository : Repository<Location>,  ILocationRepository
    {
        public LocationRepository(WhereHouseContext context)
            :base(context)
        {

        }

        public Location GetByItemId(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
