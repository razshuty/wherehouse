﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using WhereHouse.Domain.ValueObjects;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.Migrations
{
    [DbContext(typeof(WhereHouseContext))]
    partial class WhereHouseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WhereHouse.Domain.Entities.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Email")
                        .HasMaxLength(255);

                    b.Property<string>("LogoImgUrl")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<Guid>("OwnerId");

                    b.Property<string>("SupportEmail")
                        .HasMaxLength(255);

                    b.Property<string>("WebSite")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Customer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Location", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CompanyId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Locations");
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Supplier", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Suppliers");
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Company", b =>
                {
                    b.OwnsOne("WhereHouse.Domain.ValueObjects.Address", "Address", b1 =>
                        {
                            b1.Property<Guid>("CompanyId");

                            b1.Property<string>("City");

                            b1.Property<string>("Country");

                            b1.Property<string>("Number");

                            b1.Property<string>("PostalCode");

                            b1.Property<string>("Province");

                            b1.Property<string>("Street");

                            b1.ToTable("Companies");

                            b1.HasOne("WhereHouse.Domain.Entities.Company")
                                .WithOne("Address")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.Address", "CompanyId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("WhereHouse.Domain.ValueObjects.PhoneNumber", "PhoneNumber", b1 =>
                        {
                            b1.Property<Guid>("CompanyId");

                            b1.Property<int?>("AreaCode");

                            b1.Property<int?>("CountryCode");

                            b1.Property<int?>("Extention");

                            b1.Property<int?>("Number");

                            b1.ToTable("Companies");

                            b1.HasOne("WhereHouse.Domain.Entities.Company")
                                .WithOne("PhoneNumber")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.PhoneNumber", "CompanyId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Customer", b =>
                {
                    b.OwnsOne("WhereHouse.Domain.ValueObjects.Address", "Address", b1 =>
                        {
                            b1.Property<Guid?>("CustomerId");

                            b1.Property<string>("City");

                            b1.Property<string>("Country");

                            b1.Property<string>("Number");

                            b1.Property<string>("PostalCode");

                            b1.Property<string>("Province");

                            b1.Property<string>("Street");

                            b1.ToTable("Customers");

                            b1.HasOne("WhereHouse.Domain.Entities.Customer")
                                .WithOne("Address")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.Address", "CustomerId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("WhereHouse.Domain.ValueObjects.Contact", "Contact", b1 =>
                        {
                            b1.Property<Guid>("CustomerId");

                            b1.Property<string>("Data");

                            b1.Property<int>("Type");

                            b1.ToTable("Customers");

                            b1.HasOne("WhereHouse.Domain.Entities.Customer")
                                .WithOne("Contact")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.Contact", "CustomerId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("WhereHouse.Domain.ValueObjects.FullName", "FullName", b1 =>
                        {
                            b1.Property<Guid>("CustomerId");

                            b1.Property<string>("FirstName");

                            b1.Property<string>("LastName");

                            b1.Property<string>("Title");

                            b1.ToTable("Customers");

                            b1.HasOne("WhereHouse.Domain.Entities.Customer")
                                .WithOne("FullName")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.FullName", "CustomerId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Location", b =>
                {
                    b.HasOne("WhereHouse.Domain.Entities.Company")
                        .WithMany("Locations")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("WhereHouse.Domain.Entities.Supplier", b =>
                {
                    b.OwnsOne("WhereHouse.Domain.ValueObjects.Address", "Address", b1 =>
                        {
                            b1.Property<Guid?>("SupplierId");

                            b1.Property<string>("City");

                            b1.Property<string>("Country");

                            b1.Property<string>("Number");

                            b1.Property<string>("PostalCode");

                            b1.Property<string>("Province");

                            b1.Property<string>("Street");

                            b1.ToTable("Suppliers");

                            b1.HasOne("WhereHouse.Domain.Entities.Supplier")
                                .WithOne("Address")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.Address", "SupplierId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("WhereHouse.Domain.ValueObjects.Contact", "Contact", b1 =>
                        {
                            b1.Property<Guid?>("SupplierId");

                            b1.Property<string>("Data");

                            b1.Property<int>("Type");

                            b1.ToTable("Suppliers");

                            b1.HasOne("WhereHouse.Domain.Entities.Supplier")
                                .WithOne("Contact")
                                .HasForeignKey("WhereHouse.Domain.ValueObjects.Contact", "SupplierId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
