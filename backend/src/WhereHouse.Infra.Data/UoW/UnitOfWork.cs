﻿using WhereHouse.Domain.Core.Commands;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Infra.Data.Context;

namespace WhereHouse.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WhereHouseContext _context;

        public UnitOfWork(WhereHouseContext context)
        {
            _context = context;
        }

        public CommandResponse Commit()
        {
            var rowsAffected = _context.SaveChanges();
            return new CommandResponse(rowsAffected > 0);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
