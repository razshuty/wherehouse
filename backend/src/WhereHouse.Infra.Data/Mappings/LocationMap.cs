using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Infra.Data.Mappings
{
    public class LocationMap : EntityMap<Location>
    {
        public override void Map(EntityTypeBuilder<Location> builder)
        {
            base.Map(builder);

            builder.Property(p => p.CompanyId)
                .IsRequired();

            builder.Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}