using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Infra.Data.Mappings
{
    public class SupplierMap : EntityMap<Supplier>
    {
        public override void Map(EntityTypeBuilder<Supplier> builder)
        {
            base.Map(builder);

            builder.Property(p => p.Name)
                .IsRequired();

            builder.OwnsOne(p => p.Contact);
            builder.OwnsOne(p => p.Address);
        }
    }
}