using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WhereHouse.Domain.Core.Models;
using WhereHouse.Infra.Data.Extensions;

namespace WhereHouse.Infra.Data.Mappings
{
    public abstract class EntityMap<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity : Entity
    {
        public override void Map(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Ignore(c => c.ValidationResult);
        }
    }
}