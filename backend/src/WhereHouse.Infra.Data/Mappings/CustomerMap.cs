using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Infra.Data.Mappings
{
    public class CustomerMap : EntityMap<Customer>
    {
        public override void Map(EntityTypeBuilder<Customer> builder)
        {
            base.Map(builder);

            builder.OwnsOne(c => c.FullName);
            builder.OwnsOne(c => c.Address);
            builder.OwnsOne(c => c.Contact);
        }
    }
}