using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Infra.Data.Mappings
{
    public class CompanyMap : EntityMap<Company>
    {
        public override void Map(EntityTypeBuilder<Company> builder)
        {
            base.Map(builder);

            builder.Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder.OwnsOne(c => c.Address);
            builder.OwnsOne(c => c.PhoneNumber);

            builder.Property(c => c.Email)
                .HasMaxLength(255);

            builder.Property(c => c.SupportEmail)
                .HasMaxLength(255);

            builder.Property(c => c.WebSite)
                .HasMaxLength(255);

            builder.Property(c => c.LogoImgUrl)
                .HasMaxLength(255);

            builder.HasMany(e => e.Locations)
                .WithOne()
                .HasForeignKey(e => e.CompanyId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}