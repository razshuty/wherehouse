﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace WhereHouse.Infra.Data.Context
{
    public class WhereHouseNoSQLContext : IDisposable
    {
        public readonly IMongoDatabase Caching;

        public WhereHouseNoSQLContext()
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            // define the database to use
            var connectionString = config.GetConnectionString("WhereHouseNoSQLContext");

            var client = new MongoClient(connectionString);

            Caching = client.GetDatabase("caching");
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}