﻿using System.IO;
using FluentValidation.Results;
using WhereHouse.Infra.Data.Mappings;
using WhereHouse.Infra.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Infra.Data.Context
{
    public class WhereHouseContext : DbContext
    {
        // Just for Migrations
        public DbSet<Company> Companies { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore(typeof(ValidationResult));

            modelBuilder.AddConfiguration(new CompanyMap());
            modelBuilder.AddConfiguration(new LocationMap());
            modelBuilder.AddConfiguration(new CustomerMap());
            modelBuilder.AddConfiguration(new SupplierMap());

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            
            // define the database to use
            optionsBuilder.UseSqlServer(config.GetConnectionString("WhereHouseContext"));
        }
    }
}
