﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UPCItemDb;
using WhereHouse.Infra.UPCItemRepository.Interfaces;

namespace WhereHouse.Infra.UPCItemRepository
{
    public class UPCItemRepository
    {
        private readonly UPCItemDBClient _upcItemDbClient;
        private readonly IUPCItemDbRepository _upcItemDbRepository;

        public UPCItemRepository(IUPCItemDbRepository upcItemDbRepository)
        {
            _upcItemDbClient = new UPCItemDBClient();
            _upcItemDbRepository = upcItemDbRepository;
        }

        public async Task<IEnumerable<UPCItem>> Lookup(params string[] codes)
        {
            var items = await _upcItemDbClient.LookupByPostAsync(codes);
            return items.Items.Select(UPCItem.ToUPCItem);
        }
        public async Task<IEnumerable<UPCItem>> Search(string keywork)
        {
            var items = await _upcItemDbClient.SearchByPostAsync(keywork);
            return items.Items.Select(UPCItem.ToUPCItem);
        }
    }
}
