﻿namespace WhereHouse.Infra.UPCItemRepository
{
    public class UPCItem
    {
        public string[] Images { get; set; }
        public string Currency { get; set; }
        public string Weight { get; set; }
        public string Dimension { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Brand { get; set; }
        public string Description { get; set; }
        public string ASIN { get; set; }
        public string GTIN { get; set; }
        public string UPC { get; set; }
        public string Title { get; set; }
        public string EAN { get; set; }
        public string Model { get; set; }

        public static UPCItem ToUPCItem(UPCItemDb.Responses.Item item)
        {
            return new UPCItem
            {
                Images = item.Images,
                Currency = item.Currency,
                Weight = item.Weight,
                Dimension = item.Dimension,
                Size = item.Size,
                Color = item.Color,
                Brand = item.Brand,
                Description = item.Description,
                ASIN = item.ASIN,
                GTIN = item.GTIN,
                UPC = item.UPC,
                Title = item.Title,
                EAN = item.EAN,
                Model = item.Model
            };
        }
    }
}