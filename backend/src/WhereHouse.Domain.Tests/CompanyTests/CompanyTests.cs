using System.Data.Common;
using FluentAssertions;
using Xunit;

namespace WhereHouse.Domain.Tests.CompanyTests
{
    [Collection(nameof(CompanyCollection))]
    public class CompanyTests
    {
        public CompanyTestsFixture Fixture { get; set; }

        public CompanyTests(CompanyTestsFixture fixture)
        {
            Fixture = fixture;
        }

        // AAA == Arrange, Act, Assert
        [Fact(DisplayName = "New Company Valid")]
        [Trait("Category", "Company Tests")]
        public void Company_NewCompany_ShouldBeValid()
        {
            // Arrange
            var company = Fixture.GetValidCompany();

            // Act
            var result = company.IsValid();

            // Assert Fluent Assertions (more expressive)
            result.Should().BeTrue();
            company.ValidationResult.Errors.Should().HaveCount(0);

            // Assert XUnit
            Assert.True(result);
            Assert.Equal(0, company.ValidationResult.Errors.Count);
        }

        [Fact(DisplayName = "New Company Invalid")]
        [Trait("Category", "Company Tests")]
        public void Company_NewCompany_ShouldBeInvalid()
        {
            // Arrange
            var company = Fixture.GetInvalidCompany();

            // Act
            var result = company.IsValid();

            // Assert Fluent Assertions (more expressive)
            result.Should().BeFalse();
            company.ValidationResult.Errors.Should().HaveCount(c => c > 0);

            // Assert XUnit
            Assert.False(result);
            Assert.NotEqual(0, company.ValidationResult.Errors.Count);
        }
    }
}
