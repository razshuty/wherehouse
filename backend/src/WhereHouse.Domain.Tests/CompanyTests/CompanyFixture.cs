﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using MediatR;
using Moq;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using Xunit;

namespace WhereHouse.Domain.Tests.CompanyTests
{
    [CollectionDefinition(nameof(CompanyCollection))]
    public class CompanyCollection : ICollectionFixture<CompanyTestsFixture>
    {
    }

    public class CompanyTestsFixture : IDisposable
    {
        public Mock<ICompanyRepository> CompanyRepositoryMock { get; set; }
        public Mock<IMediator> MediatorMock { get; set; }

        public Company GetValidCompany()
        {
            return GenerateCompany(1, true).First();
        }

        public Company GetInvalidCompany()
        {
            var companyTests = new Faker<Company>()
                .CustomInstantiator(f => new Company(
                    Guid.NewGuid(),
                    string.Empty,
                    Guid.Empty));

            return companyTests;
        }

        public IEnumerable<Company> GetMixedLocations()
        {
            var companies = new List<Company>();

            companies.AddRange(GenerateCompany(50, true).ToList());
            companies.AddRange(GenerateCompany(50, false).ToList());

            return companies;
        }

        private static IEnumerable<Company> GenerateCompany(int number, bool isActive)
        {
            var companies = new Faker<Company>()
                .CustomInstantiator(f => new Company(
                    Guid.NewGuid(),
                    f.Company.CompanyName(),
                    Guid.NewGuid()));

            return companies.Generate(number);
        }

        public void Dispose()
        {
            // Dispose what you have!
        }
    }
}