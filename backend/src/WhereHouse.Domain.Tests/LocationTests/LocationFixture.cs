﻿using Bogus;
using Bogus.DataSets;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using Xunit;

namespace WhereHouse.Domain.Tests.Locations
{
    [CollectionDefinition(nameof(LocationCollection))]
    public class LocationCollection : ICollectionFixture<LocationTestsFixture>
    {
    }

    public class LocationTestsFixture : IDisposable
    {
        public Mock<ILocationRepository> LocationRepositoryMock { get; set; }
        public Mock<IMediator> MediatorMock { get; set; }

        public Location GetValidLocation()
        {
            return GenerateLocation(1, true).First();
        }

        public Location GetInvalidLocation()
        {
            var locationTests = new Faker<Location>()
                .CustomInstantiator(f =>
                    new Location(Guid.NewGuid(), string.Empty, Guid.Empty));

            return locationTests;
        }

        public IEnumerable<Location> GetMixedLocations()
        {
            var locations = new List<Location>();

            locations.AddRange(GenerateLocation(50, true).ToList());
            locations.AddRange(GenerateLocation(50, false).ToList());

            return locations;
        }

        private static IEnumerable<Location> GenerateLocation(int number, bool isActive)
        {
            var locationTests = new Faker<Location>()
                .CustomInstantiator(f => new Location(
                    Guid.NewGuid(),
                    f.Company.CompanyName(),
                    Guid.NewGuid()));

            return locationTests.Generate(number);
        }

        public void Dispose()
        {
            // Dispose what you have!
        }
    }
}