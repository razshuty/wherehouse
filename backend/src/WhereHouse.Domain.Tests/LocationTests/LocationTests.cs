using FluentAssertions;
using WhereHouse.Domain.Tests.Locations;
using Xunit;

namespace WhereHouse.Domain.Tests.LocationTests
{
    [Collection(nameof(LocationCollection))]
    public class LocationTests
    {
        public LocationTestsFixture Fixture { get; set; }

        public LocationTests(LocationTestsFixture fixture)
        {
            Fixture = fixture;
        }

        // AAA == Arrange, Act, Assert
        [Fact(DisplayName = "New Location Valid")]
        [Trait("Category", "Location Tests")]
        public void Location_NewLocation_ShouldBeValid()
        {
            // Arrange
            var Location = Fixture.GetValidLocation();

            // Act
            var result = Location.IsValid();

            // Assert Fluent Assertions (more expressive)
            result.Should().BeTrue();
            Location.ValidationResult.Errors.Should().HaveCount(0);

            // Assert XUnit
            Assert.True(result);
            Assert.Equal(0, Location.ValidationResult.Errors.Count);
        }

        [Fact(DisplayName = "New Location Invalid")]
        [Trait("Category", "Location Tests")]
        public void Location_NewLocation_ShouldBeInvalid()
        {
            // Arrange
            var Location = Fixture.GetInvalidLocation();

            // Act
            var result = Location.IsValid();

            // Assert Fluent Assertions (more expressive)
            result.Should().BeFalse();
            Location.ValidationResult.Errors.Should().HaveCount(c => c > 0);

            // Assert XUnit
            Assert.False(result);
            Assert.NotEqual(0, Location.ValidationResult.Errors.Count);
        }
    }
}
