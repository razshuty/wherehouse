﻿using System;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Infra.AspNetIdentity
{
    public class AspNetUser : IUser
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IMemoryCache _cache;
        private readonly IConfiguration _configuration;

        public AspNetUser(IHttpContextAccessor accessor, IMemoryCache cache, IConfiguration configuration)
        {
            _accessor = accessor;
            _cache = cache;
            _configuration = configuration;
        }

        public Guid Id => GetCurrentUser().Result.Sub;
        public string Name => GetCurrentUser().Result.Name;

        public bool IsAuthenticated()
        {
            return GetCurrentUser().Result != null;
        }

        private async Task<UserInfo> GetCurrentUser()
        {
            if (!_accessor.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues token))
                return null;

            if (_cache.TryGetValue(token, out UserInfo userInfo))
                return userInfo;

            var identityServerClient = await DiscoveryClient.GetAsync(_configuration["IdentityServer:Authority"]);

            userInfo = await new Url(identityServerClient.UserInfoEndpoint)
                .WithHeader("Authorization", token)
                .GetAsync()
                .ReceiveJson<UserInfo>();

            _cache.Set(token, userInfo, TimeSpan.FromSeconds(double.Parse(_configuration["ApiApp:UserInfoCacheExpirationSeconds"])));

            return userInfo;
        }
        public class UserInfo
        {
            public Guid Sub { get; set; }
            public string Name { get; set; }
        }
    }
}
