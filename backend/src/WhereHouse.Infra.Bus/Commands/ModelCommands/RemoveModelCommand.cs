﻿using System;
using WhereHouse.Infra.Bus.Validators.ModelValidators;

namespace WhereHouse.Infra.Bus.Commands.ModelCommands
{
    public class RemoveModelCommand : ModelCommand
    {
        public RemoveModelCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveModelCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}