﻿using System;
using WhereHouse.Infra.Bus.Validators.ItemValidators;

namespace WhereHouse.Infra.Bus.Commands.ItemCommands
{
    public class CreateItemCommand : ItemCommand
    {
        public CreateItemCommand(decimal amount)
        {
            Id = Guid.NewGuid();
            
            Amount = amount;
            CreatedDate = DateTime.UtcNow;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}