﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Infra.Bus.Commands.ItemCommands
{
    public abstract class ItemCommand : Command
    {
        public Guid Id { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}