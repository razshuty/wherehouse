﻿using System;
using WhereHouse.Infra.Bus.Validators.ItemValidators;

namespace WhereHouse.Infra.Bus.Commands.ItemCommands
{
    public class DeleteItemCommand : ItemCommand
    {
        public DeleteItemCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}