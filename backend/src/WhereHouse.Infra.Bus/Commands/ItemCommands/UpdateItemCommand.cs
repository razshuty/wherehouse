﻿using System;
using WhereHouse.Infra.Bus.Validators.ItemValidators;

namespace WhereHouse.Infra.Bus.Commands.ItemCommands
{
    public class UpdateItemCommand : ItemCommand
    {
        public UpdateItemCommand(Guid id, decimal amount, DateTime createdDate)
        {
            Id = id;
            Amount = amount;
            CreatedDate = createdDate;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}