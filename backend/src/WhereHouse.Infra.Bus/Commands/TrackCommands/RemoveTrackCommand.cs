﻿using System;
using WhereHouse.Infra.Bus.Validators.TrackValidators;

namespace WhereHouse.Infra.Bus.Commands.TrackCommands
{
    public class RemoveTrackCommand : TrackCommand
    {
        public RemoveTrackCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveTrackCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}