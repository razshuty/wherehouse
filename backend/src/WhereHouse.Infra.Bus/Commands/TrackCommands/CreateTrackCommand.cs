﻿using System;
using WhereHouse.Infra.Bus.Validators.TrackValidators;

namespace WhereHouse.Infra.Bus.Commands.TrackCommands
{
    public class CreateTrackCommand : TrackCommand
    {
        public CreateTrackCommand(string name, string type)
        {
            Id = Guid.NewGuid();
            Name = name;
            Type = type;
            CreatedDate = DateTime.UtcNow;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateTrackCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}