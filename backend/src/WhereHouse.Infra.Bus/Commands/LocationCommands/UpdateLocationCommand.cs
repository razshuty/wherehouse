﻿using System;
using WhereHouse.Infra.Bus.Validators.LocationValidators;

namespace WhereHouse.Infra.Bus.Commands.LocationCommands
{
    public class UpdateLocationCommand : LocationCommand
    {
        public UpdateLocationCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}