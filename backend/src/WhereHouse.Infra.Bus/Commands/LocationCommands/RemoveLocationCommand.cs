﻿using System;
using WhereHouse.Infra.Bus.Validators.LocationValidators;

namespace WhereHouse.Infra.Bus.Commands.LocationCommands
{
    public class RemoveLocationCommand : LocationCommand
    {
        public RemoveLocationCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}