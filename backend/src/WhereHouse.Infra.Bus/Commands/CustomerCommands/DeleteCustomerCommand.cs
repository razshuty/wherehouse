﻿using System;
using WhereHouse.Infra.Bus.Validators.CustomerValidators;

namespace WhereHouse.Infra.Bus.Commands.CustomerCommands
{
    public class DeleteCustomerCommand : CustomerCommand
    {
        public DeleteCustomerCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveCustomerCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}