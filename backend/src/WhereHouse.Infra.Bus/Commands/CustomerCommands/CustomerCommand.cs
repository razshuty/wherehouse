﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Infra.Bus.Commands.CustomerCommands
{
    public abstract class CustomerCommand : Command
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}