﻿using System;
using WhereHouse.Infra.Bus.Validators.UPCItemValidators;

namespace WhereHouse.Infra.Bus.Commands.UPCItemCommands
{
    public class CreateUPCItemCommand : UPCItemCommand
    {
        public CreateUPCItemCommand(string name)
        {
            Id = Guid.NewGuid();
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateUPCItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}