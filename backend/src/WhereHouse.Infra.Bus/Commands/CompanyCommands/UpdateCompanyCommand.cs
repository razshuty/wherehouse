﻿using System;
using WhereHouse.Infra.Bus.Validators.CompanyValidators;

namespace WhereHouse.Infra.Bus.Commands.CompanyCommands
{
    public class UpdateCompanyCommand : CompanyCommand
    {
        public UpdateCompanyCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }       

        public override bool IsValid()
        {
            ValidationResult = new UpdateCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}