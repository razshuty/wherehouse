﻿using System;
using WhereHouse.Domain.Core.Commands;
using WhereHouse.Infra.Bus.Validators.CompanyValidators;

namespace WhereHouse.Infra.Bus.Commands.CompanyCommands
{
    public class ChangeCompanyOwnershipCommand : Command
    {
        public Guid CompanyId { get; set; }
        public Guid CurrentOwnerId { get; set; }
        public Guid NewOwnerId { get; set; }


        public override bool IsValid()
        {
            ValidationResult = new ChangeCompanyOwnershipCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}