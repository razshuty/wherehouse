﻿using System;
using WhereHouse.Infra.Bus.Validators.CompanyValidators;

namespace WhereHouse.Infra.Bus.Commands.CompanyCommands
{
    public class DeleteCompanyCommand : CompanyCommand
    {
        public DeleteCompanyCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}