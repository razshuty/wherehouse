﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.LocationEvents
{
    public class LocationUpdatedEvent : Event
    {
        public LocationUpdatedEvent(Guid id, string name, DateTime createdDate)
        {
            Id = id;
            Name = name;
            CreatedDate = createdDate;
            AggregateId = id;
        }
        public Guid Id { get; }

        public string Name { get; }

        public DateTime CreatedDate { get; }
    }
}