﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.LocationEvents
{
    public class LocationRemovedEvent : Event
    {
        public LocationRemovedEvent(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public Guid Id { get; set; }
    }
}