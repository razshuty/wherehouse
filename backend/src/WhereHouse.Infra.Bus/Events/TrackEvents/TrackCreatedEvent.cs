﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.TrackEvents
{
    public class TrackCreatedEvent : Event
    {
        public TrackCreatedEvent(Guid id, string name, string type, DateTime createdDate)
        {
            Id = id;
            Name = name;
            Type = type;
            CreatedDate = createdDate;
            AggregateId = id;
        }
        public Guid Id { get; set; }

        public string Name { get; private set; }

        public string Type { get; private set; }

        public DateTime CreatedDate { get; private set; }
    }
}