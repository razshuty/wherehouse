﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.ModelEvents
{
    public class ModelCreatedEvent : Event
    {
        public ModelCreatedEvent(Guid id, string name, string partNumber, DateTime createdDate)
        {
            Id = id;
            Name = name;
            PartNumber = partNumber;
            CreatedDate = createdDate;
            AggregateId = id;
        }
        public Guid Id { get; set; }

        public string Name { get; private set; }

        public string PartNumber { get; private set; }

        public DateTime CreatedDate { get; private set; }
    }
}