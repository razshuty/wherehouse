﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.ItemEvents
{
    public class ItemCreatedEvent : Event
    {
        public ItemCreatedEvent(Guid id, string model, decimal amount, DateTime createdDate)
        {
            Id = id;
            Model = model;
            Amount = amount;
            CreatedDate = createdDate;
            AggregateId = id;
        }
        public Guid Id { get; set; }

        public string Model { get; private set; }

        public decimal Amount { get; private set; }

        public DateTime CreatedDate { get; private set; }
    }
}