﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.ItemEvents
{
    public class ItemRemovedEvent : Event
    {
        public ItemRemovedEvent(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public Guid Id { get; set; }
    }
}