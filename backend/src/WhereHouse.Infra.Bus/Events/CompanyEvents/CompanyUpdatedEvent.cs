﻿using System;
using WhereHouse.Domain.Core.Events;

namespace WhereHouse.Infra.Bus.Events.CompanyEvents
{
    public class CompanyUpdatedEvent : Event
    {
        public CompanyUpdatedEvent(Guid id, string name, DateTime createdDate)
        {
            Id = id;
            Name = name;
            CreatedDate = createdDate;
            AggregateId = id;
        }
        public Guid Id { get; set; }

        public string Name { get; private set; }

        public DateTime CreatedDate { get; private set; }
    }
}