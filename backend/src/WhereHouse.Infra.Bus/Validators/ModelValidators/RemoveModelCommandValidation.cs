using WhereHouse.Infra.Bus.Commands.ModelCommands;

namespace WhereHouse.Infra.Bus.Validators.ModelValidators
{
    public class RemoveModelCommandValidator : ModelValidator<RemoveModelCommand>
    {
        public RemoveModelCommandValidator()
        {
            ValidateId();
        }
    }
}