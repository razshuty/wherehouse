using WhereHouse.Infra.Bus.Commands.ModelCommands;

namespace WhereHouse.Infra.Bus.Validators.ModelValidators
{
    public class UpdateModelCommandValidator : ModelValidator<UpdateModelCommand>
    {
        public UpdateModelCommandValidator()
        {
            ValidateId();
            ValidateName();
            ValidatePartNumber();
        }
    }
}