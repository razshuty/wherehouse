using WhereHouse.Infra.Bus.Commands.ModelCommands;

namespace WhereHouse.Infra.Bus.Validators.ModelValidators
{
    public class CreateModelCommandValidator : ModelValidator<CreateModelCommand>
    {
        public CreateModelCommandValidator()
        {
            ValidateName();
        }
    }
}