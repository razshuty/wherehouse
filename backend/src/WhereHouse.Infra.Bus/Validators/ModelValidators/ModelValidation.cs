using System;
using FluentValidation;
using WhereHouse.Infra.Bus.Commands.ModelCommands;

namespace WhereHouse.Infra.Bus.Validators.ModelValidators
{
    public abstract class ModelValidator<T> : AbstractValidator<T> where T : ModelCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidatePartNumber()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the PartNumber")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}