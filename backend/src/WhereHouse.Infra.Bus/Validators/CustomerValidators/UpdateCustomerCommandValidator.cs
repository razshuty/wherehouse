﻿using WhereHouse.Infra.Bus.Commands.CustomerCommands;

namespace WhereHouse.Infra.Bus.Validators.CustomerValidators
{
    public class UpdateCustomerCommandValidator : CustomerCommandValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerCommandValidator()
        {
            ValidateId();
            ValidateFirstName();
            ValidateLastName();
            ValidateEmail();
        }
    }
}