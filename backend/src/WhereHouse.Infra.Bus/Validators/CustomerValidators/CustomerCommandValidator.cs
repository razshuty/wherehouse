using System;
using FluentValidation;
using WhereHouse.Infra.Bus.Commands.CustomerCommands;

namespace WhereHouse.Infra.Bus.Validators.CustomerValidators
{
    public abstract class CustomerCommandValidator<T> : AbstractValidator<T> where T : CustomerCommand
    {
        protected void ValidateFirstName()
        {
            RuleFor(c => c.FirstName)
                .NotEmpty().WithMessage("Invalid first name")
                .Length(2, 20).WithMessage("The first name must have between 2 and 20 characters");
        }

        protected void ValidateLastName()
        {
            RuleFor(c => c.LastName)
                .NotEmpty().WithMessage("Invalid last name")
                .Length(2, 50).WithMessage("The first name must have between 2 and 50 characters");
        }

        protected void ValidateEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Invalid email")
                .Length(5, 255).WithMessage("The email must have between 2 and 255 characters");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}