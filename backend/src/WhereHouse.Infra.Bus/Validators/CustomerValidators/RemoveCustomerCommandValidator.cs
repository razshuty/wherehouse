﻿using WhereHouse.Infra.Bus.Commands.CustomerCommands;

namespace WhereHouse.Infra.Bus.Validators.CustomerValidators
{
    public class RemoveCustomerCommandValidator : CustomerCommandValidator<DeleteCustomerCommand>
    {
        public RemoveCustomerCommandValidator()
        {
            ValidateId();
        }
    }
}