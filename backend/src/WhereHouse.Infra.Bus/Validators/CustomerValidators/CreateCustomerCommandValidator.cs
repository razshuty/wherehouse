
using WhereHouse.Infra.Bus.Commands.CustomerCommands;

namespace WhereHouse.Infra.Bus.Validators.CustomerValidators
{
    public class CreateCustomerCommandValidator : CustomerCommandValidator<CreateCustomerCommand>
    {
        public CreateCustomerCommandValidator()
        {
            ValidateFirstName();
            ValidateLastName();
            ValidateEmail();
        }
    }
}