using FluentValidation;
using WhereHouse.Infra.Bus.Commands.UPCItemCommands;

namespace WhereHouse.Infra.Bus.Validators.UPCItemValidators
{
    public abstract class UPCItemCommandValidator<T> : AbstractValidator<T> where T : UPCItemCommand
    {
    }
}