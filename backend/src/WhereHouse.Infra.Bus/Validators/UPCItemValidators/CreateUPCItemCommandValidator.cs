﻿using WhereHouse.Infra.Bus.Commands.UPCItemCommands;

namespace WhereHouse.Infra.Bus.Validators.UPCItemValidators
{
    public class CreateUPCItemCommandValidator : UPCItemCommandValidator<CreateUPCItemCommand>
    {
        public CreateUPCItemCommandValidator()
        {
        }
    }
}