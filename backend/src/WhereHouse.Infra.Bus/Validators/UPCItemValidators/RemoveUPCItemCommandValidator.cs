﻿using WhereHouse.Infra.Bus.Commands.UPCItemCommands;

namespace WhereHouse.Infra.Bus.Validators.UPCItemValidators
{
    public class RemoveUPCItemCommandValidator : UPCItemCommandValidator<RemoveUPCItemCommand>
    {
        public RemoveUPCItemCommandValidator()
        {
        }
    }
}