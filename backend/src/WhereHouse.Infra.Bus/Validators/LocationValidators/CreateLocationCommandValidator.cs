
using WhereHouse.Infra.Bus.Commands.LocationCommands;

namespace WhereHouse.Infra.Bus.Validators.LocationValidators
{
    public class CreateLocationCommandValidator : LocationCommandValidator<CreateLocationCommand>
    {
        public CreateLocationCommandValidator()
        {
            ValidateCompanyId();
            ValidateName();
        }
    }
}