using System;
using FluentValidation;
using WhereHouse.Infra.Bus.Commands.LocationCommands;

namespace WhereHouse.Infra.Bus.Validators.LocationValidators
{
    public abstract class LocationCommandValidator<T> : AbstractValidator<T> where T : LocationCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateCompanyId()
        {
            RuleFor(c => c.CompanyId)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        protected static bool HaveMinimumAge(DateTime birthDate)
        {
            return birthDate <= DateTime.Now.AddYears(-18);
        }
    }
}