﻿using WhereHouse.Infra.Bus.Commands.LocationCommands;

namespace WhereHouse.Infra.Bus.Validators.LocationValidators
{
    public class UpdateLocationCommandValidator : LocationCommandValidator<UpdateLocationCommand>
    {
        public UpdateLocationCommandValidator()
        {
            ValidateId();
            ValidateName();
        }
    }
}