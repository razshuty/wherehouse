﻿using WhereHouse.Infra.Bus.Commands.LocationCommands;

namespace WhereHouse.Infra.Bus.Validators.LocationValidators
{
    public class RemoveLocationCommandValidator : LocationCommandValidator<RemoveLocationCommand>
    {
        public RemoveLocationCommandValidator()
        {
            ValidateId();
        }
    }
}