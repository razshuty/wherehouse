﻿using WhereHouse.Infra.Bus.Commands.TrackCommands;

namespace WhereHouse.Infra.Bus.Validators.TrackValidators
{
    public class CreateTrackCommandValidator : TrackValidator<CreateTrackCommand>
    {
        public CreateTrackCommandValidator()
        {
            ValidateName();
        }
    }
}