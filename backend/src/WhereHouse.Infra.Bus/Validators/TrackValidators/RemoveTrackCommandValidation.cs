﻿using WhereHouse.Infra.Bus.Commands.TrackCommands;

namespace WhereHouse.Infra.Bus.Validators.TrackValidators
{
    public class RemoveTrackCommandValidator : TrackValidator<RemoveTrackCommand>
    {
        public RemoveTrackCommandValidator()
        {
            ValidateId();
        }
    }
}