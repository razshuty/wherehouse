﻿using WhereHouse.Infra.Bus.Commands.CompanyCommands;

namespace WhereHouse.Infra.Bus.Validators.CompanyValidators
{
    public class UpdateCompanyCommandValidator : CompanyCommandValidator<UpdateCompanyCommand>
    {
        public UpdateCompanyCommandValidator()
        {
            ValidateId();
            ValidateName();
        }
    }
}