using System;
using FluentValidation;
using WhereHouse.Infra.Bus.Commands.CompanyCommands;

namespace WhereHouse.Infra.Bus.Validators.CompanyValidators
{
    public abstract class CompanyCommandValidator<T> : AbstractValidator<T> where T : CompanyCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateOwnerId()
        {
            RuleFor(c => c.OwnerId)
                .NotEqual(Guid.Empty);
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}