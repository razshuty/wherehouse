﻿using WhereHouse.Infra.Bus.Commands.CompanyCommands;

namespace WhereHouse.Infra.Bus.Validators.CompanyValidators
{
    public class RemoveCompanyCommandValidator : CompanyCommandValidator<DeleteCompanyCommand>
    {
        public RemoveCompanyCommandValidator()
        {
            ValidateId();
        }
    }
}