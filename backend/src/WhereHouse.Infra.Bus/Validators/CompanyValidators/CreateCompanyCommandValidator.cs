
using WhereHouse.Infra.Bus.Commands.CompanyCommands;

namespace WhereHouse.Infra.Bus.Validators.CompanyValidators
{
    public class CreateCompanyCommandValidator : CompanyCommandValidator<CreateCompanyCommand>
    {
        public CreateCompanyCommandValidator()
        {
            ValidateOwnerId();
            //ValidateId();
            ValidateName();
        }
    }
}