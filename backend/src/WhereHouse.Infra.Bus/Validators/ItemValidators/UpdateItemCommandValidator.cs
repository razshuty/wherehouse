﻿using WhereHouse.Infra.Bus.Commands.ItemCommands;

namespace WhereHouse.Infra.Bus.Validators.ItemValidators
{
    public class UpdateItemCommandValidator : ItemValidator<UpdateItemCommand>
    {
        public UpdateItemCommandValidator()
        {
            ValidateId();
        }
    }
}