﻿using System;
using FluentValidation;
using WhereHouse.Infra.Bus.Commands.ItemCommands;

namespace WhereHouse.Infra.Bus.Validators.ItemValidators
{
    public abstract class ItemValidator<T> : AbstractValidator<T> where T : ItemCommand
    {
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}