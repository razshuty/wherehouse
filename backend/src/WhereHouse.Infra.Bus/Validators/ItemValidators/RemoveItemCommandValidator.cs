﻿using WhereHouse.Infra.Bus.Commands.ItemCommands;

namespace WhereHouse.Infra.Bus.Validators.ItemValidators
{
    public class RemoveItemCommandValidator : ItemValidator<DeleteItemCommand>
    {
        public RemoveItemCommandValidator()
        {
            ValidateId();
        }
    }
}