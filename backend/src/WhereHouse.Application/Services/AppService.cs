﻿using System;
using AutoMapper;
using FluentValidation.Results;
using WhereHouse.Application.Interfaces;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Application.Services
{
    public abstract class AppService : IAppService
    {
        protected readonly IUser CurentUser;
        protected readonly IMapper Mapper;

        public ValidationResult ValidationResult { get; protected set; }

        protected AppService(IMapper mapper, IUser user)
        {
            Mapper = mapper;
            CurentUser = user;

            ValidationResult = new ValidationResult();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
