﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using UPCItemDb;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Application.Services
{
    public class UPCItemAppService : AppService, IUPCItemAppService
    {
        private readonly IUPCItemRepository _upcItemRepository;
        private readonly UPCItemDBClient _upcItemDbClient;

        public UPCItemAppService(
            IUPCItemRepository upcItemRepository,
            UPCItemDBClient upcItemDbClient,
            IMapper mapper, IUser user) : base(mapper, user)
        {
            _upcItemRepository = upcItemRepository;
            _upcItemDbClient = upcItemDbClient;
        }

        public async Task<IEnumerable<UPCItemViewModel>> Lookup(params string[] codes)
        {
            var items = _upcItemRepository.Find(item => codes.Contains(item.UPC) || codes.Contains(item.EAN)).ToList();
            if (items.Count == codes.Length) return Mapper.Map<IEnumerable<UPCItemViewModel>>(items);

            var missingCodes = codes.Where(code => items.All(item => item.UPC != code && item.EAN != code)).ToArray();

            var missingItems = await _upcItemDbClient.LookupByGetAsync(missingCodes);
            foreach (var item in missingItems.Items)
            {
                var newUPCItem = Mapper.Map<UPCItem>(item);
                _upcItemRepository.Add(newUPCItem);

                items.Add(newUPCItem);
            }

            return Mapper.Map<IEnumerable<UPCItemViewModel>>(items);
        }

        public async Task<IEnumerable<UPCItemViewModel>> Search(string keyword)
        {
            var items = await _upcItemDbClient.SearchByPostAsync(keyword);
            return Mapper.Map<IEnumerable<UPCItemViewModel>>(items);
        }
    }
}