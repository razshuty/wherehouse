﻿using System;
using System.Collections.Generic;
using AutoMapper;
using WhereHouse.Application.Commands.CompanyCommands;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Application.Services
{
    public class CompanyAppService : AppService, ICompanyAppService
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyAppService(
            ICompanyRepository companyRepository,
            IMapper mapper, IUser user) : base(mapper,  user)
        {
            _companyRepository = companyRepository;
        }

        public IEnumerable<CompanyViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<CompanyViewModel>>(_companyRepository.GetAll());
        }

        public CompanyViewModel GetById(Guid id)
        {
            return Mapper.Map<CompanyViewModel>(_companyRepository.GetById(id));
        }

        public bool Create(CreateCompanyCommand command)
        {
            command.OwnerId = CurentUser.Id;

            var newCompany = Mapper.Map<Company>(command);

            if (!newCompany.IsValid())
            {
                ValidationResult = newCompany.ValidationResult;
                return false;
            }

            _companyRepository.Add(newCompany);
            return true;
        }

        public bool Update(UpdateCompanyCommand command)
        {
            var company = _companyRepository.GetById(command.Id);
            if (company == null) return false;

            company.TryRename(command.Name);

            if (!company.IsValid())
            {
                ValidationResult = company.ValidationResult;
                return false;
            }

            _companyRepository.Update(company);
            return true;
        }

        public bool Delete(DeleteCompanyCommand command)
        {
            throw new NotImplementedException();
        }

        public bool ChangeOwnership(ChangeCompanyOwnershipCommand command)
        {
            var company = _companyRepository.GetById(command.CompanyId);
            if (company == null)
            {
                return false;
            }

            company.TryChangeOwnership(command.NewOwnerId);

            if (!company.IsValid())
            {
                ValidationResult = company.ValidationResult;
                return false;
            }

            _companyRepository.Update(company);
            return true;
        }
    }
}
