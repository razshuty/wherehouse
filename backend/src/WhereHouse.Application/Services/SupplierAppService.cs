﻿using System;
using System.Collections.Generic;
using AutoMapper;
using WhereHouse.Application.Commands.SupplierCommands;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Application.Services
{
    public class SupplierAppService : AppService, ISupplierAppService
    {
        private readonly ISupplierRepository _supplierRepository;

        public SupplierAppService(
            ISupplierRepository supplierRepository,
            IMapper mapper, IUser user) : base(mapper, user)
        {
            _supplierRepository = supplierRepository;
        }

        public IEnumerable<SupplierViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<SupplierViewModel>>(_supplierRepository.GetAll());
        }

        public SupplierViewModel GetById(Guid id)
        {
            return Mapper.Map<SupplierViewModel>(_supplierRepository.GetById(id));
        }

        public bool Create(CreateSupplierCommand command)
        {
            var newSupplier = Mapper.Map<Supplier>(command);

            if (!newSupplier.IsValid())
            {
                ValidationResult = newSupplier.ValidationResult;
                return false;
            }

            _supplierRepository.Add(newSupplier);
            return true;
        }

        public bool Update(UpdateSupplierCommand command)
        {
            var supplier = _supplierRepository.GetById(command.Id);
            if (supplier == null) return false;

            if (!supplier.TryRename(command.Name))
            {
                ValidationResult = supplier.ValidationResult;
                return ValidationResult.IsValid;
            }

            if (!supplier.IsValid())
            {
                ValidationResult = supplier.ValidationResult;
                return ValidationResult.IsValid;
            }

            _supplierRepository.Add(supplier);
            return true;
        }

        public bool Delete(DeleteSupplierCommand command)
        {
            var supplier = _supplierRepository.GetById(command.Id);
            if (supplier == null) return false;

            if (!supplier.CanBeDelete())
            {
                ValidationResult = supplier.ValidationResult;
                return false;
            }

            _supplierRepository.Remove(supplier.Id);
            return true;
        }
    }
}