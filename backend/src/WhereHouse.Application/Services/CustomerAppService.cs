﻿using System;
using System.Collections.Generic;
using AutoMapper;
using WhereHouse.Application.Commands.CustomerCommands;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;

namespace WhereHouse.Application.Services
{
    public class CustomerAppService : AppService, ICustomerAppService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerAppService(
            ICustomerRepository customerRepository,
            IMapper mapper, IUser user) : base(mapper, user)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<CustomerViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<CustomerViewModel>>(_customerRepository.GetAll());
        }

        public CustomerViewModel GetById(Guid id)
        {
            return Mapper.Map<CustomerViewModel>(_customerRepository.GetById(id));
        }

        public bool Create(CreateCustomerCommand command)
        {
            var newCustomer = Mapper.Map<Customer>(command);

            if (!newCustomer.IsValid())
            {
                ValidationResult = newCustomer.ValidationResult;
                return false;
            }

            _customerRepository.Add(newCustomer);
            return true;
        }

        public bool Update(UpdateCustomerCommand command)
        {
            var customer = _customerRepository.GetById(command.Id);

            if (customer == null) return false;

            // TODO: Create the try command
            //customer.TryUpdate(command);

            if (!customer.IsValid())
            {
                ValidationResult = customer.ValidationResult;
                return false;
            }

            _customerRepository.Add(customer);
            return true;
        }

        public bool Delete(DeleteCustomerCommand command)
        {
            var customer = _customerRepository.GetById(command.Id);
            if (customer == null) return false;

            if (!customer.CanBeDelete())
            {
                ValidationResult = customer.ValidationResult;
                return false;
            }

            _customerRepository.Remove(customer.Id);
            return true;
        }
    }
}
