﻿using System;
using System.Collections.Generic;
using AutoMapper;
using WhereHouse.Application.Commands.LocationCommands;
using WhereHouse.Application.Interfaces;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;
using WhereHouse.Domain.Interfaces;
using WhereHouse.Domain.Queries;

namespace WhereHouse.Application.Services
{
    public class LocationAppService : AppService, ILocationAppService
    {
        private readonly IMapper _mapper;
        private readonly ILocationRepository _locationRepository;

        public LocationAppService(IMapper mapper,
                                  ILocationRepository locationRepository,
                                  IUser user) : base(mapper, user)
        {
            _mapper = mapper;
            _locationRepository = locationRepository;
        }

        public IEnumerable<LocationViewModel> GetAll()
        {
            return _mapper.Map<IEnumerable<LocationViewModel>>(_locationRepository.GetAll());
        }

        public IEnumerable<LocationViewModel> GetAllFromCompany(Guid companyId)
        {
            var locations = _locationRepository.Find(LocationQueries.FindByCompanyId(companyId));
            return _mapper.Map<IEnumerable<LocationViewModel>>(locations);
        }

        public LocationViewModel GetById(Guid id)
        {
            return _mapper.Map<LocationViewModel>(_locationRepository.GetById(id));
        }

        public bool Create(CreateLocationCommand command)
        {
            var newLocation = Mapper.Map<Location>(command);

            if (!newLocation.IsValid())
            {
                ValidationResult = newLocation.ValidationResult;
                return false;
            }

            _locationRepository.Add(newLocation);
            return true;

        }

        public bool Update(UpdateLocationCommand command)
        {
            var location = _locationRepository.GetById(command.Id);

            if (location== null) return false;

            // TODO: Create the try command
            //location.TryUpdate(command);

            if (!location.IsValid())
            {
                ValidationResult = location.ValidationResult;
                return false;
            }

            _locationRepository.Add(location);
            return true;
        }

        public bool Delete(RemoveLocationCommand command)
        {
            var location = _locationRepository.GetById(command.Id);
            if (location == null) return false;

            if (!location.CanBeDelete())
            {
                ValidationResult = location.ValidationResult;
                return false;
            }

            _locationRepository.Remove(location.Id);
            return true;

        }
    }
}
