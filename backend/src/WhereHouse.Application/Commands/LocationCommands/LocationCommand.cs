﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Application.Commands.LocationCommands
{
    public abstract class LocationCommand : Command
    {
        public Guid CompanyId { get; set; }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}