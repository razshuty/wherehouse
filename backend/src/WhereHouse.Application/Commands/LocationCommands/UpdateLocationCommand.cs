﻿using System;
using WhereHouse.Application.Validators.LocationValidators;

namespace WhereHouse.Application.Commands.LocationCommands
{
    public class UpdateLocationCommand : LocationCommand
    {
        public UpdateLocationCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}