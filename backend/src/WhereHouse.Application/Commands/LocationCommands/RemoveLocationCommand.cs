﻿using System;
using WhereHouse.Application.Validators.LocationValidators;

namespace WhereHouse.Application.Commands.LocationCommands
{
    public class RemoveLocationCommand : LocationCommand
    {
        public RemoveLocationCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}