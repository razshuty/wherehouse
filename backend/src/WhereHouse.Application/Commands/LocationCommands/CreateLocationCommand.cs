﻿using System;
using WhereHouse.Application.Validators.LocationValidators;

namespace WhereHouse.Application.Commands.LocationCommands
{
    public class CreateLocationCommand : LocationCommand
    {
        public CreateLocationCommand(Guid companyId, string name)
        {
            CompanyId = companyId;
            Id = Guid.NewGuid();
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateLocationCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}