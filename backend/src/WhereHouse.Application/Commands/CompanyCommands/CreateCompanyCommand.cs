﻿using System;
using WhereHouse.Application.Validators.CompanyValidators;

namespace WhereHouse.Application.Commands.CompanyCommands
{
    public class CreateCompanyCommand : CompanyCommand
    {
        public CreateCompanyCommand(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}