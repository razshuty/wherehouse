﻿using System;
using WhereHouse.Application.Validators.CompanyValidators;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Application.Commands.CompanyCommands
{
    public class ChangeCompanyOwnershipCommand : Command
    {
        public Guid CompanyId { get; set; }
        public Guid CurrentOwnerId { get; set; }
        public Guid NewOwnerId { get; set; }


        public override bool IsValid()
        {
            ValidationResult = new ChangeCompanyOwnershipCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}