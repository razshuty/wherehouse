﻿using System;
using WhereHouse.Application.Validators.CompanyValidators;

namespace WhereHouse.Application.Commands.CompanyCommands
{
    public class UpdateCompanyCommand : CompanyCommand
    {
        public UpdateCompanyCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }       

        public override bool IsValid()
        {
            ValidationResult = new UpdateCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}