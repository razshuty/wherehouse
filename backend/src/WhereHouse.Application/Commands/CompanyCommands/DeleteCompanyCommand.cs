﻿using System;
using WhereHouse.Application.Validators.CompanyValidators;

namespace WhereHouse.Application.Commands.CompanyCommands
{
    public class DeleteCompanyCommand : CompanyCommand
    {
        public DeleteCompanyCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveCompanyCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}