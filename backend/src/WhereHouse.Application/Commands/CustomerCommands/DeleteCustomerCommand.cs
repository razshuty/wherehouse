﻿using System;
using WhereHouse.Application.Validators.CustomerValidators;

namespace WhereHouse.Application.Commands.CustomerCommands
{
    public class DeleteCustomerCommand : CustomerCommand
    {
        public DeleteCustomerCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveCustomerCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}