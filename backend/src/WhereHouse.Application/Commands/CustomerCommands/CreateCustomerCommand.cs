﻿using System;
using WhereHouse.Application.Validators.CustomerValidators;

namespace WhereHouse.Application.Commands.CustomerCommands
{
    public class CreateCustomerCommand : CustomerCommand
    {
        public CreateCustomerCommand(string firstName, string lastName, string email)
        {
            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateCustomerCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}