﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Application.Commands.UPCItemCommands
{
    public abstract class UPCItemCommand : Command
    {
        public Guid Id { get; set; }
        public string[] Images { get; set; }
        public string Currency { get; set; }
        public string Weight { get; set; }
        public string Dimension { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Brand { get; set; }
        public string Description { get; set; }
        public string ASIN { get; set; }
        public string GTIN { get; set; }
        public string UPC { get; set; }
        public string Title { get; set; }
        public string EAN { get; set; }
        public string Model { get; set; }
    }
}