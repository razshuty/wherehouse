﻿using System;
using WhereHouse.Application.Validators.UPCItemValidators;

namespace WhereHouse.Application.Commands.UPCItemCommands
{
    public class CreateUPCItemCommand : UPCItemCommand
    {
        public CreateUPCItemCommand(string name)
        {
            Id = Guid.NewGuid();
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateUPCItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}