﻿using System;
using WhereHouse.Application.Validators.UPCItemValidators;

namespace WhereHouse.Application.Commands.UPCItemCommands
{
    public class RemoveUPCItemCommand : UPCItemCommand
    {
        public RemoveUPCItemCommand(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveUPCItemCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}