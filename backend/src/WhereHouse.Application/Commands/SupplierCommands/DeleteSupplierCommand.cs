﻿using System;
using WhereHouse.Application.Validators.SupplierValidators;

namespace WhereHouse.Application.Commands.SupplierCommands
{
    public class DeleteSupplierCommand : SupplierCommand
    {
        public DeleteSupplierCommand(Guid id)
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveSupplierCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}