﻿using System;
using WhereHouse.Domain.Core.Commands;

namespace WhereHouse.Application.Commands.SupplierCommands
{
    public abstract class SupplierCommand : Command
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}