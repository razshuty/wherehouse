﻿using System;
using WhereHouse.Application.Validators.SupplierValidators;

namespace WhereHouse.Application.Commands.SupplierCommands
{
    public class CreateSupplierCommand : SupplierCommand
    {
        public CreateSupplierCommand(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateSupplierCommandValidator().Validate(this);
            return ValidationResult.IsValid;            
        }
    }
}