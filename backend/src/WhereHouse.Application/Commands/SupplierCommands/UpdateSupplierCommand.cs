﻿using System;
using WhereHouse.Application.Validators.SupplierValidators;

namespace WhereHouse.Application.Commands.SupplierCommands
{
    public class UpdateSupplierCommand : SupplierCommand
    {
        public UpdateSupplierCommand(Guid id, string name)
        {
            Id = id;
            Name = name;
        }       

        public override bool IsValid()
        {
            ValidationResult = new UpdateSupplierCommandValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}