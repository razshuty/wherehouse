﻿using System;
using FluentValidation.Results;

namespace WhereHouse.Application.Interfaces
{
    public interface IAppService : IDisposable
    {
        ValidationResult ValidationResult { get; }
    }
}