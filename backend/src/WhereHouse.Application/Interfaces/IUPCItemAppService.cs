﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.Interfaces
{
    public interface IUPCItemAppService : IAppService
    {
        Task<IEnumerable<UPCItemViewModel>> Lookup(params string[] codes);
        Task<IEnumerable<UPCItemViewModel>> Search(string keyword);
    }
}
