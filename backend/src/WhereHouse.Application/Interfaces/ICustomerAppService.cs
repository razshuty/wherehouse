﻿using System;
using System.Collections.Generic;
using WhereHouse.Application.Commands.CustomerCommands;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.Interfaces
{
    public interface ICustomerAppService : IAppService
    {
        IEnumerable<CustomerViewModel> GetAll();
        CustomerViewModel GetById(Guid id);
        bool Create(CreateCustomerCommand command);
        bool Update(UpdateCustomerCommand command);
        bool Delete(DeleteCustomerCommand command);
    }
}
