﻿using System;
using System.Collections.Generic;
using WhereHouse.Application.Commands.SupplierCommands;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.Interfaces
{
    public interface ISupplierAppService : IAppService
    {
        IEnumerable<SupplierViewModel> GetAll();
        SupplierViewModel GetById(Guid id);
        bool Create(CreateSupplierCommand command);
        bool Update(UpdateSupplierCommand command);
        bool Delete(DeleteSupplierCommand command);
    }
}
