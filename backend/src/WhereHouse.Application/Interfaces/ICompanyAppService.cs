﻿using System;
using System.Collections.Generic;
using WhereHouse.Application.Commands.CompanyCommands;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.Interfaces
{
    public interface ICompanyAppService : IAppService
    {
        IEnumerable<CompanyViewModel> GetAll();
        CompanyViewModel GetById(Guid id);

        bool Create(CreateCompanyCommand command);
        bool Update(UpdateCompanyCommand command);
        bool Delete(DeleteCompanyCommand command);

        bool ChangeOwnership(ChangeCompanyOwnershipCommand command);
    }
}
