﻿using System;
using System.Collections.Generic;
using WhereHouse.Application.Commands.LocationCommands;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.Interfaces
{
    public interface ILocationAppService : IAppService
    {
        IEnumerable<LocationViewModel> GetAll();
        IEnumerable<LocationViewModel> GetAllFromCompany(Guid id);
        LocationViewModel GetById(Guid id);
        bool Create(CreateLocationCommand command);
        bool Update(UpdateLocationCommand command);
        bool Delete(RemoveLocationCommand command);
    }
}
