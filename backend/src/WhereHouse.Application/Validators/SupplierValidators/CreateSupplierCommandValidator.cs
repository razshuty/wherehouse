
using WhereHouse.Application.Commands.SupplierCommands;

namespace WhereHouse.Application.Validators.SupplierValidators
{
    public class CreateSupplierCommandValidator : SupplierCommandValidator<CreateSupplierCommand>
    {
        public CreateSupplierCommandValidator()
        {
            ValidateName();
        }
    }
}