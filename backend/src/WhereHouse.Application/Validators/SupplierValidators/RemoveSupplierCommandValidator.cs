﻿using WhereHouse.Application.Commands.SupplierCommands;

namespace WhereHouse.Application.Validators.SupplierValidators
{
    public class RemoveSupplierCommandValidator : SupplierCommandValidator<DeleteSupplierCommand>
    {
        public RemoveSupplierCommandValidator()
        {
            ValidateId();
        }
    }
}