﻿using WhereHouse.Application.Commands.SupplierCommands;

namespace WhereHouse.Application.Validators.SupplierValidators
{
    public class UpdateSupplierCommandValidator : SupplierCommandValidator<UpdateSupplierCommand>
    {
        public UpdateSupplierCommandValidator()
        {
            ValidateId();
            ValidateName();
        }
    }
}