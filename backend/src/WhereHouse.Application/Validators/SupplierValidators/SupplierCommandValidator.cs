using System;
using FluentValidation;
using WhereHouse.Application.Commands.SupplierCommands;

namespace WhereHouse.Application.Validators.SupplierValidators
{
    public abstract class SupplierCommandValidator<T> : AbstractValidator<T> where T : SupplierCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Invalid name")
                .Length(2, 20).WithMessage("The name must have between 2 and 20 characters");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}