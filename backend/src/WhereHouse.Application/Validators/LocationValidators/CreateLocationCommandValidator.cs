
using WhereHouse.Application.Commands.LocationCommands;

namespace WhereHouse.Application.Validators.LocationValidators
{
    public class CreateLocationCommandValidator : LocationCommandValidator<CreateLocationCommand>
    {
        public CreateLocationCommandValidator()
        {
            ValidateCompanyId();
            ValidateName();
        }
    }
}