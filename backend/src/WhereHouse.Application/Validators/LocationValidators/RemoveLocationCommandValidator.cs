﻿using WhereHouse.Application.Commands.LocationCommands;

namespace WhereHouse.Application.Validators.LocationValidators
{
    public class RemoveLocationCommandValidator : LocationCommandValidator<RemoveLocationCommand>
    {
        public RemoveLocationCommandValidator()
        {
            ValidateId();
        }
    }
}