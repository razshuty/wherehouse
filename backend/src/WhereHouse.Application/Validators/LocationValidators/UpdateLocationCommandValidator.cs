﻿using WhereHouse.Application.Commands.LocationCommands;

namespace WhereHouse.Application.Validators.LocationValidators
{
    public class UpdateLocationCommandValidator : LocationCommandValidator<UpdateLocationCommand>
    {
        public UpdateLocationCommandValidator()
        {
            ValidateId();
            ValidateName();
        }
    }
}