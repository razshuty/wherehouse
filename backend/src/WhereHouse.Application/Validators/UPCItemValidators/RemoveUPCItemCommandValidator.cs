﻿using WhereHouse.Application.Commands.UPCItemCommands;

namespace WhereHouse.Application.Validators.UPCItemValidators
{
    public class RemoveUPCItemCommandValidator : UPCItemCommandValidator<RemoveUPCItemCommand>
    {
        public RemoveUPCItemCommandValidator()
        {
        }
    }
}