﻿using WhereHouse.Application.Commands.UPCItemCommands;

namespace WhereHouse.Application.Validators.UPCItemValidators
{
    public class CreateUPCItemCommandValidator : UPCItemCommandValidator<CreateUPCItemCommand>
    {
        public CreateUPCItemCommandValidator()
        {
        }
    }
}