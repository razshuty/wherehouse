using FluentValidation;
using WhereHouse.Application.Commands.UPCItemCommands;

namespace WhereHouse.Application.Validators.UPCItemValidators
{
    public abstract class UPCItemCommandValidator<T> : AbstractValidator<T> where T : UPCItemCommand
    {
    }
}