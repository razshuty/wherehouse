﻿using WhereHouse.Application.Commands.CompanyCommands;

namespace WhereHouse.Application.Validators.CompanyValidators
{
    public class UpdateCompanyCommandValidator : CompanyCommandValidator<UpdateCompanyCommand>
    {
        public UpdateCompanyCommandValidator()
        {
            ValidateId();
            ValidateName();
        }
    }
}