﻿using WhereHouse.Application.Commands.CompanyCommands;

namespace WhereHouse.Application.Validators.CompanyValidators
{
    public class RemoveCompanyCommandValidator : CompanyCommandValidator<DeleteCompanyCommand>
    {
        public RemoveCompanyCommandValidator()
        {
            ValidateId();
        }
    }
}