﻿using System;
using FluentValidation;
using WhereHouse.Application.Commands.CompanyCommands;

namespace WhereHouse.Application.Validators.CompanyValidators
{
    public class ChangeCompanyOwnershipCommandValidator : AbstractValidator<ChangeCompanyOwnershipCommand>
    {
        public ChangeCompanyOwnershipCommandValidator()
        {
            RuleFor(p => p.CompanyId)
                .NotNull()
                .NotEqual(Guid.Empty);

            RuleFor(p => p.CurrentOwnerId)
                .NotNull()
                .NotEqual(Guid.Empty);

            RuleFor(p => p.NewOwnerId)
                .NotNull()
                .NotEqual(Guid.Empty);
        }
    }
}