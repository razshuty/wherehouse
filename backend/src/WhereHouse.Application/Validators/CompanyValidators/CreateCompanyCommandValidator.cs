
using WhereHouse.Application.Commands.CompanyCommands;

namespace WhereHouse.Application.Validators.CompanyValidators
{
    public class CreateCompanyCommandValidator : CompanyCommandValidator<CreateCompanyCommand>
    {
        public CreateCompanyCommandValidator()
        {
            ValidateOwnerId();
            //ValidateId();
            ValidateName();
        }
    }
}