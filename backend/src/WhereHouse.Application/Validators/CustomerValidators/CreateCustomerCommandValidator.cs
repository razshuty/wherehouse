
using WhereHouse.Application.Commands.CustomerCommands;

namespace WhereHouse.Application.Validators.CustomerValidators
{
    public class CreateCustomerCommandValidator : CustomerCommandValidator<CreateCustomerCommand>
    {
        public CreateCustomerCommandValidator()
        {
            ValidateFirstName();
            ValidateLastName();
            ValidateEmail();
        }
    }
}