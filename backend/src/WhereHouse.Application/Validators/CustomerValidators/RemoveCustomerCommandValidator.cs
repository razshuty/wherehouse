﻿using WhereHouse.Application.Commands.CustomerCommands;

namespace WhereHouse.Application.Validators.CustomerValidators
{
    public class RemoveCustomerCommandValidator : CustomerCommandValidator<DeleteCustomerCommand>
    {
        public RemoveCustomerCommandValidator()
        {
            ValidateId();
        }
    }
}