﻿using WhereHouse.Application.Commands.CustomerCommands;

namespace WhereHouse.Application.Validators.CustomerValidators
{
    public class UpdateCustomerCommandValidator : CustomerCommandValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerCommandValidator()
        {
            ValidateId();
            ValidateFirstName();
            ValidateLastName();
            ValidateEmail();
        }
    }
}