﻿using AutoMapper;
using WhereHouse.Application.Commands.CompanyCommands;
using WhereHouse.Application.Commands.LocationCommands;
using WhereHouse.Application.ViewModels;

namespace WhereHouse.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<CompanyViewModel, CreateCompanyCommand>()
                .ConstructUsing(c => new CreateCompanyCommand(c.Name));
            CreateMap<CompanyViewModel, UpdateCompanyCommand>()
                .ConstructUsing(c => new UpdateCompanyCommand(c.Id, c.Name));
            CreateMap<CompanyViewModel, DeleteCompanyCommand>()
                .ConstructUsing(c => new DeleteCompanyCommand(c.Id));

            CreateMap<LocationViewModel, CreateLocationCommand>()
                .ConstructUsing(c => new CreateLocationCommand(c.CompanyId, c.Name));
            CreateMap<LocationViewModel, UpdateLocationCommand>()
                .ConstructUsing(c => new UpdateLocationCommand(c.Id, c.Name));
            CreateMap<LocationViewModel, RemoveLocationCommand>()
                .ConstructUsing(c => new RemoveLocationCommand(c.Id));
        }
    }
}
