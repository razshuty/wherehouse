﻿using AutoMapper;
using WhereHouse.Application.ViewModels;
using WhereHouse.Domain.Entities;

namespace WhereHouse.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Company, CompanyViewModel>();
            CreateMap<Location, LocationViewModel>();
            CreateMap<UPCItem, UPCItemViewModel>();
            CreateMap<UPCItemDb.Responses.Item, UPCItem>();
        }
    }
}
